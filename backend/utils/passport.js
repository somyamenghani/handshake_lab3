"use strict";
var JwtStrategy = require("passport-jwt").Strategy;
var ExtractJwt = require("passport-jwt").ExtractJwt;
const passport = require("passport");
var { secret } = require("../config/config");
const Students = require('../models/students');
const kafka = require('../kafka/client');
// Setup work and export for the JWT passport strategy
function auth() {
    var opts = {
        jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt"),
        secretOrKey: secret
    };
    passport.use(
        new JwtStrategy(opts, (jwt_payload, done) => {
            kafka.make_request('passport', jwt_payload.id, function (err, results) {
                if (err) {
                  return done(err, false);
                }
                if(results){
                  done(null, results);
                }
                else {
                  done(null, false);
                }
              })
        })
    )
}

exports.auth = auth;
exports.checkAuth = passport.authenticate("jwt", { session: false });