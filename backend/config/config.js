"use strict";
const config = {
   secret: "cmpe273_kafka_passport_mongo",
   awsBucket: "handshakepublic",
   awsSecretAccessKey: '',
   awsAccessKey:'',
   region: 'us-east-1',
   awsSessionToken:'',
   awsPermission: "public-read"
};

module.exports = config;