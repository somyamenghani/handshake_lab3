const Students = require('../models/students');

const updateStudent = async (args) => {
console.log("reaching update",args)
  let student= await Students.findOne({ _id:args.studentId})
       
        if (!student) {
            return{
          status : 401,
          message : "No such user exists"
            }
        }
        else {

           let studentUpdate=await Students.findOneAndUpdate({ _id: args.studentId },
                {
                  name: args.name,
                  careerObjective:args.careerObjective,
                  skills:args.skills
                },
                {
                  new: true
                });
                if(studentUpdate)
                {
                    return{
                        status : 200,
                        message : "Updated student information"
                          }  
                }
                else{
                    return{
                    status :500,
                    message : "Internal server error"
                    }
                }
            }  
}

const addEducation = async (args) => {
  console.log("reaching add education",args)
    let student= await Students.findOne({ _id:args.userId})
         
          if (!student) {
              return{
            status : 401,
            message : "No such user exists"
              }
          }
          else {
            
           
               var newEducationDetails = {
                   collegeName:args.userCollege,
                   location: args.userCollegeLocation,
                   degree:args.userDegree,
                   major:args.userMajor,
                   yop:args.userYOP,
                   cgpa:args.userCgpa}
                 student.educationalDetails.push(newEducationDetails);
                 let savedEducation= await  student.save();
                 if (savedEducation) {
                  return { status: 200, message: "EDUCATION_ADDED" };
              }
              else {
                  return { status: 500, message: "INTERNAL_SERVER_ERROR" };
              }     
            }
         
          
  }
  const addExperience = async (args) => {
    console.log("reaching add experience",args)
    let student= await Students.findOne({ _id:args.userId})
         
    if (!student) {
        return{
      status : 401,
      message : "No such user exists"
        }
    }
    else {
      
     
         var newExperienceDetails = {
          companyName:args.companyName,
          location: args.location,
          title:args.title,
          startDate:args.startDate,
          endDate:args.endDate,
          workDescription:args.workDescription
        }
        student.experienceDetails.push(newExperienceDetails);}
           
           let savedExperience= await  student.save();
           if (savedExperience) {
            return { status: 200, message: "Experience_ADDED" };
        }
        else {
            return { status: 500, message: "INTERNAL_SERVER_ERROR" };
        }     
      }
    

exports.updateStudent=updateStudent;
exports.addEducation=addEducation;
exports.addExperience=addExperience;