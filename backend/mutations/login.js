
const Students = require('../models/students');
const Companies = require('../models/companies');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const { secret } = require('../config/config');

const login = async (args) => {
    console.log("reached here login",args)
    if(args.userType=='1')
    {
    let student = await Students.findOne({ emailId: args.emailId });
    if (!student) {
        return { status: 401, message: "NO_USER" };
    }
    if (passwordHash.verify(args.password, student.password)) {
        const payload = { user_id: student._id, name: student.name, emailId:student.emailId};
        var token = jwt.sign(payload, secret, {
            expiresIn: 1008000
        });
        token = 'JWT ' + token;
        return { status: 200, message: token };
    }
    else {
        return { status: 401, message: "INCORRECT_PASSWORD" };
    }
}
else
{
    let company = await Companies.findOne({ emailId: args.emailId });
    if (!company) {
        return { status: 401, message: "NO_USER" };
    }
    if (passwordHash.verify(args.password, company.password)) {
        const payload = { user_id: company._id, name: company.name, emailId:company.emailId};
        var token = jwt.sign(payload, secret, {
            expiresIn: 1008000
        });
        token = 'JWT ' + token;
        return { status: 200, message: token };
    }
    else {
        return { status: 401, message: "INCORRECT_PASSWORD" };
    }
}
}

exports.login = login;