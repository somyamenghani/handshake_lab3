const Companies = require('../models/companies');

const updateCompany = async (args) => {
console.log("reaching update",args)
  let company= await Companies.findOne({ _id:args.companyId})
       
        if (!company) {
            return{
          status : 401,
          message : "No such user exists"
            }
        }
        else {

           let companyUpdate=await Companies.findOneAndUpdate({ _id: args.companyId },
                {
                  name: args.companyname,
                  location: args.location,
                  description: args.description
                },
                {
                  new: true
                });
                if(companyUpdate)
                {
                    return{
                        status : 200,
                        message : "Updated company information"
                          }  
                }
                else{
                    return{
                    status :500,
                    message : "Internal server error"
                    }
                }
            }  
}

exports.updateCompany=updateCompany;