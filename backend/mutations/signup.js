const passwordHash = require('password-hash');
const Students = require('../models/students');
const Companies = require('../models/companies');

const studentSignup=async(args)=>{
    console.log("args reached"+JSON.stringify(args))
    let hashedPassword = passwordHash.generate(args.password);
    var newStudent = new Students({
        emailId: args.emailId,
        password: hashedPassword,
        name: args.name,
        collegeName:args.collegeName,
        major:args.major,
        skills:""
      });

    let student= await Students.find({ emailId: args.emailId});
       
    if (student.length) {
        return { status: 400, message: "USER_EXISTS" };
    }
    let savedStudent = await newStudent.save();
    if (savedStudent) {
        return { status: 200, message: "USER_ADDED" };
    }
    else {
        return { status: 500, message: "INTERNAL_SERVER_ERROR" };
    }
}

const companySignup=async(args)=>{
    console.log("args reached"+JSON.stringify(args))
    let hashedPassword = passwordHash.generate(args.password);
    var newCompany = new Companies({
        emailId: args.emailId,
        password: hashedPassword,
        name: args.name,
        location: args.location
      });

    let company= await Companies.find({ emailId: args.emailId});
       
    if (company.length) {
        return { status: 400, message: "COMPANY_EXISTS" };
    }
    let savedCompany = await newCompany.save();
    if (savedCompany) {
        return { status: 200, message: "COMPANY_ADDED" };
    }
    else {
        return { status: 500, message: "INTERNAL_SERVER_ERROR" };
    }
}

exports.studentSignup = studentSignup;
exports.companySignup = companySignup;