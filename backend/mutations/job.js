const Jobs = require('../models/job');
const Applications =require('../models/application');

const addJob = async (args) => {

    var newJob = new Jobs({
        companyId: args.companyId,
    jobtitle: args.jobtitle,
    postingdate:new Date().toISOString().slice(0,10),
    deadline:args.deadline,
    city:args.city,
    state:args.state,
    salary:args.salary,
    description:args.description,
    jobcategory:args.jobcategory,
    companyname:args.companyname
      });
     
       let savedJob= await  newJob.save();
       if (savedJob) {
        return { status: 200, message: "JOB_ADDED" };
    }
    else {
        return { status: 500, message: "INTERNAL_SERVER_ERROR" };
    }     
}

const searchJob = async (args) => {
   console.log(args,"in job search")
   let jobs=await Jobs.find({})
   let filteredJob = jobs.filter(job => job.companyname.toLowerCase().includes(args.searchString.toLowerCase()) || job.jobtitle.toLowerCase().includes(args.searchString.toLowerCase()))
   if(filteredJob)
   {
       console.log(filteredJob);
    return { status: 200, message:JSON.stringify(filteredJob) };
   }
   else{
    return { status: 500, message: "INTERNAL_SERVER_ERROR" };
   }
    
}

const applyJob = async (args) => {

    
        var newApplication = new Applications({
            jobid:args.jobid,
            studentid: args.studentid,
        studentname: args.studentname,
        applicationdate:new Date().toISOString().slice(0,10),
        status:'Pending',
        jobtitle:args.jobtitle,
       
        
          });
         
          let savedApplication= await  newApplication.save();
          if (savedApplication) {
           return { status: 200, message: "JOB_APPLIED" };
       }
       else {
           return { status: 500, message: "INTERNAL_SERVER_ERROR" };
       }     
}

exports.addJob=addJob;
exports.searchJob=searchJob;
exports.applyJob=applyJob;