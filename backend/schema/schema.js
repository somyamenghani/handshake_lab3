const graphql = require('graphql');
const Student = require("../models/students");
const Company = require('../models/companies');
const {studentSignup} = require('../mutations/signup');
const {companySignup} = require('../mutations/signup');
const {login} = require('../mutations/login');
const{addJob} = require('../mutations/job');
const{searchJob} = require('../mutations/job');
const{applyJob} = require('../mutations/job');
const{updateCompany} = require('../mutations/updateCompany');
const{updateStudent,addEducation,addExperience} = require('../mutations/updateStudent');
const Jobs = require('../models/job');
const Applications = require('../models/application');

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull
} = graphql;


const EducationType = new GraphQLObjectType({
    name: 'Education',
    fields: () => ({
        id: { type: GraphQLID },
        _id:{ type:GraphQLString  },
        collegeName:{ type:GraphQLString  },
        major:{ type:GraphQLString  },
        degree:{ type:GraphQLString  },
        cgpa:{ type:GraphQLString  },
        location:{ type:GraphQLString  },
        yop:{ type:GraphQLString  }
    })
});
const ExperienceType = new GraphQLObjectType({
    name: 'Experience',
    fields: () => ({
        id: { type: GraphQLID },
        userId:{ type:GraphQLString  },
                companyName:{ type:GraphQLString  },
                title:{ type:GraphQLString  },
                location:{ type:GraphQLString  },
                startDate:{ type:GraphQLString  },
                endDate:{ type:GraphQLString  },
                workDescription:{ type:GraphQLString  }
    })
});
const UserType = new GraphQLObjectType({
    name: 'Student',
    fields: () => ({
        id: { type: GraphQLID },
        name: {type: GraphQLString},
    password: {type: GraphQLString},
    emailId:{type: GraphQLString},
    collegeName:{type: GraphQLString},
    major:{type: GraphQLString},
    careerObjective:{type: GraphQLString},
    contactNumber:{type: GraphQLString},
    skills:{type: GraphQLString},
   educationalDetails:{type:new GraphQLList(EducationType)},
   experienceDetails:{type:new GraphQLList(ExperienceType)},
    })
});
const CompanyType = new GraphQLObjectType({
    name: 'Company',
    fields: () => ({
        id: { type: GraphQLID },
        name: {type: GraphQLString},
    password: {type: GraphQLString},
    emailId:{type: GraphQLString},
    location:{type: GraphQLString},
    description:{type:GraphQLString}
    })
});
const JobType = new GraphQLObjectType({
    name: 'Job',
    fields: () => ({
        _id: { type: GraphQLString },
        
        companyId: {type: GraphQLString},
    jobtitle: {type: GraphQLString},
    postingdate:{type: GraphQLString},
    deadline:{type: GraphQLString},
    city:{type: GraphQLString},
    state:{type: GraphQLString},
    salary:{type: GraphQLString},
    description:{type: GraphQLString},
    jobcategory:{type: GraphQLString},
    companyname:{type: GraphQLString},
   
    })
});
const ApplicationType = new GraphQLObjectType({
    name: 'Application',
    fields: () => ({
        _id: { type: GraphQLString },
        
    studentid: {type: GraphQLString},
    studentname: {type: GraphQLString},
    jobid:{type: GraphQLString},
    jobtitle:{type: GraphQLString},
    applicationdate:{type: GraphQLString}
    
    })
});
const StatusType = new GraphQLObjectType({
    name: 'Status',
    fields: () => ({
        status: { type: GraphQLInt },
        message: { type: GraphQLString }
    })
});
const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        student: {
            type: UserType,
            args: { studentId: { type: GraphQLString } },
            async resolve(parent, args) {
                let student = await Student.findById(args.studentId);
                if (student) {
                    return student;
                }
            }
        },
        company: {
            type: CompanyType,
            args: { companyId: { type: GraphQLString } },
            async resolve(parent, args) {
                let company = await Company.findById(args.companyId);
                if (company) {
                    return company;
                }
            }
        },
       
                jobs: {
                    type: new GraphQLList(JobType),
                    args: { companyId: { type: GraphQLString } },
                    async resolve(parent, args) {
                        let jobs = await Jobs.find({companyId: args.companyId});
                        if (jobs) {
                            return jobs;
                        }
                    }
                },
                applications: {
                    type: new GraphQLList(ApplicationType),
                    args: { studentid: { type: GraphQLString } },
                    async resolve(parent, args) {
                        let applications = await Applications.find({studentid: args.studentid});
                        if (applications) {
                            return applications;
                        }
                    }
                },
                students: {
                    type: new GraphQLList(UserType),
                    
                    async resolve(parent, args) {
                        let students = await Student.find({});
                        if (students) {
                            return students;
                        }
                    }
                },
                jobsAll: {
                    type: new GraphQLList(JobType),
                    args: { input: { type: GraphQLString } },
                    async resolve(parent, args) {
                        if(args.input=='')
                        {
                            console.log("reached if",args)
                        let jobs = await Jobs.find({});
                        if (jobs) {
                            return jobs;
                        }
                    }
                    else
                    { console.log("reached else")
                        let jobs =await Jobs.find({$or:[{companyname:{ $regex: args.input, $options: 'i' }},{jobtitle:{ $regex: args.input, $options: 'i' }}]});
                        if(jobs)
                        {
                            return jobs;
                        }
                    }
                    }
                },
                studentSearch: {
                    type: new GraphQLList(UserType),
                    args: { input: { type: GraphQLString } },
                    async resolve(parent, args) {
                        let students = await Student.find({$or:[{name:{ $regex: args.input, $options: 'i' }},{collegeName:{ $regex: args.input, $options: 'i' }}]});
                        if (students) {
                            return students;
                        }
                    }
                }

               
    }
});
const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addStudent: {
            type: StatusType,
            args: {
                name: { type: GraphQLString },
                emailId: { type: GraphQLString },
                password: { type: GraphQLString },
                collegeName:{type: GraphQLString},
                major:{type: GraphQLString}
                
            },
            async resolve(parent, args) {
                return studentSignup(args);
            }
        },
        addCompany: {
            type: StatusType,
            args: {
                name: { type: GraphQLString },
                emailId: { type: GraphQLString },
                password: { type: GraphQLString },
                location:{type: GraphQLString}, 
            },
            async resolve(parent, args) {
                return companySignup(args);
            }
        },
        login: {
            type: StatusType,
            args: {
                emailId: { type: GraphQLString },
                password: { type: GraphQLString },
                userType:{ type:GraphQLString }
            },
            resolve(parent, args) {
                return login(args);
            }
        },
        addJob: {
            type: StatusType,
            args: {
                userType:{ type: GraphQLString },
                companyId:{ type: GraphQLString },
                companyname:{ type: GraphQLString },
                jobtitle:{ type: GraphQLString },
                description:{ type: GraphQLString },
                salary:{ type: GraphQLString },
                postingdate:{ type: GraphQLString },
                deadline:{ type: GraphQLString },
                state:{ type: GraphQLString },
                city:{ type: GraphQLString },
                jobcategory:{ type: GraphQLString }
            },
            async resolve(parent, args) {
                return addJob(args);
            }
        },
        updateCompany: {
            type: StatusType,
            args: {
            companyId: { type:GraphQLString  },
            companyname: {type: GraphQLString},
            emailId:{type: GraphQLString},
            location:{type: GraphQLString},
            description:{type:GraphQLString}
            },
            async resolve(parent, args) {
                return updateCompany(args);
            }
        },
        updateStudent: {
            type: StatusType,
            args: {
            studentId: { type:GraphQLString  },
            name: {type: GraphQLString},
            emailId:{type: GraphQLString},
            careerObjective:{type: GraphQLString},
            skills:{type: GraphQLString}

            
            },
            async resolve(parent, args) {
                return updateStudent(args);
            }
        },
        addEducation: {
            type: StatusType,
            args: {
           
            userId:{ type:GraphQLString  },
            userCollege:{ type:GraphQLString  },
            userMajor:{ type:GraphQLString  },
            userDegree:{ type:GraphQLString  },
            userCgpa:{ type:GraphQLString  },
            userCollegeLocation:{ type:GraphQLString  },
            userYOP:{ type:GraphQLString  }

            
            },
            async resolve(parent, args) {
                return addEducation(args);
            }
        },
        addExperience: {
            type: StatusType,
            args: {
                userId:{ type:GraphQLString  },
                companyName:{ type:GraphQLString  },
                title:{ type:GraphQLString  },
                location:{ type:GraphQLString  },
                startDate:{ type:GraphQLString  },
                endDate:{ type:GraphQLString  },
                workDescription:{ type:GraphQLString  }

            
            },
            async resolve(parent, args) {
                return addExperience(args);
            }
        },
        searchJob: {
            type: StatusType,
            args: 
            {searchString: {type:GraphQLString } }
            ,
            async resolve(parent, args) {
                return searchJob(args);
            }
        },
        applyJob: {
            type: StatusType,
            args: {
                studentid: { type:GraphQLString},
                studentname: {type: GraphQLString},
                jobid:{type: GraphQLString},
                jobtitle:{type: GraphQLString}
            },
            async resolve(parent, args) {
                return applyJob(args);
            }
        },
        company: {
            type: CompanyType,
            args: { companyId: { type: GraphQLString } },
            async resolve(parent, args) {
                let company = await Company.findById(args.companyId);
                if (company) {
                    return company;
                }
            }
        },
        students: {
            type: new GraphQLList(ApplicationType),
            args: { jobid: { type: GraphQLString } },
            async resolve(parent, args) {
                console.log(args)
                let students = await Applications.find({jobid:args.jobid});
                if (students) {
                    return students;
                }
            }
        },
        student: {
            type: UserType,
            args: { studentid: { type: GraphQLString } },
            async resolve(parent, args) {
                let student = await Student.findById(args.studentid);
                if (student) {
                    return student;
                }
            }
        }
    }
});
module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
});