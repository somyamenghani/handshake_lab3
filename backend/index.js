"use strict"
  const app = require("./app");
  const graphqlHTTP = require('express-graphql');
  const schema = require('./schema/schema');
  // const mongoose = require('mongoose');
// const  mongoDB  = require('./db/dbConnection');
const mongoose = require('mongoose');
const {mongoDB} = require("./config/dbconfig")
var options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  poolSize: 500,
  bufferMaxEntries: 0
};
mongoose.connect(mongoDB, options, (err, res) => {
  if (err) {
      console.log(err);
      console.log(`MongoDB Connection Failed`);
  } else {
      console.log(`MongoDB Connected`);
  }
});
  //routes
  // const login = require("./routes/login");
  // const signup = require("./routes/signup");
  // const profile = require("./routes/profile");
  // const update = require("./routes/update");
  // const companyProfile = require("./routes/companyProfile");
  // const events = require("./routes/events");
  // const jobs = require("./routes/jobs");
  // const messages = require("./routes/messages");
  

  // app.use("/login", login);
  // app.use("/signup", signup);
  // app.use("/profile", profile);
  // app.use("/update", update);
  // app.use("/companyProfile", companyProfile);
  // app.use("/events", events);
  // app.use("/jobs/",jobs);
  // app.use("/messages/",messages);

  app.use("/graphql",graphqlHTTP({
    schema,
    graphiql: true
}));
  
  app.listen(3001, () => {
    console.log(`Server listening on port 3001`);
  });
  
  module.exports = app;