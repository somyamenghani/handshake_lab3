const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var jobsSchema = new Schema({
    companyId: {type: String, required: true},
    jobtitle: {type: String, required: true},
    postingdate:{type:String,required:true},
    deadline:{type:String,required:true},
    city:{type:String,required:true},
    state:{type:String,required:true},
    salary:{type:String,required:true},
    description:{type:String,required:true},
    jobcategory:{type:String,required:true},
    companyname:{type:String,required:true},

},
{
    versionKey: false
});

module.exports = mongoose.model('job', jobsSchema);