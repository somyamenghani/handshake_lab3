const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var experienceDetailsSchema = new Schema({
    companyName: {type: String, required: true},
    title: {type: String, required: true},
    location:{type:String,required:true},
    startDate:{type:String,required:true},
    endDate:{type:String,required:true},
    workDescription:{type:String,required:true}
},
{
    versionKey: false
});

module.exports = experienceDetailsSchema;