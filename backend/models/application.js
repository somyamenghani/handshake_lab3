const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var applicationsSchema = new Schema({
    studentid: {type: String, required: true},
    studentname:{type: String, required: true},
    jobid: {type: String, required: true},
    applicationdate:{type:String,required:true},
    status:{type:String,required:true},
    jobtitle:{type:String,required:true},
    
    

},
{
    versionKey: false
});

module.exports = mongoose.model('application', applicationsSchema);