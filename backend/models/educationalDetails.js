const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var educationalDetailsSchema = new Schema({
    collegeName: {type: String, required: true},
    location: {type: String, required: true},
    degree:{type:String,required:true},
    major:{type:String,required:true},
    yop:{type:String,required:true},
    cgpa:{type:String,required:true}
},
{
    versionKey: false
});

module.exports = educationalDetailsSchema;