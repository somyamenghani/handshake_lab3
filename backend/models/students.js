const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const experienceDetailsSchema = require('./experienceDetails');
const educationalDetailsSchema = require('./educationalDetails');

var studentsSchema = new Schema({
    name: {type: String, required: true},
    password: {type: String, required: true},
    emailId:{type:String,required:true},
    collegeName:{type:String,required:true},
    major:{type:String,required:true},
    careerObjective:{type:String,required:false},
    contactNumber:{type:String,required:false},
    skills:{type:String,required:false},
    degree:{type:String,required:false},
    cgpa:{type:String,required:false},
    yop:{type:String,required:false},
    collegeLocation:{type:String,required:false},
    workDetails:{type:String,required:false},
    city:{type:String,required:false},
    state:{type:String,required:false},
    country:{type:String,required:false},
    educationalDetails:[educationalDetailsSchema],
    experienceDetails:[experienceDetailsSchema],
    
    
},
{
    versionKey: false
});

module.exports = mongoose.model('student', studentsSchema);