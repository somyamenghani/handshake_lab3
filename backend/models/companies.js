const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var companiesSchema = new Schema({
    name: {type: String, required: true},
    password: {type: String, required: true},
    emailId:{type:String,required:true},
    location:{type:String,required:true},
    contactDetails:{type:String,required:false},
    description:{type:String,required:false},
    image:{type:String,required:false}
},
{
    versionKey: false
});

module.exports = mongoose.model('company', companiesSchema);