import React, { Component } from 'react';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import Main from './components/Main';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import backendURI from './common/config';

// App Component

const client = new ApolloClient({
  uri: 'http://localhost:3001/graphql'
});

class App extends Component {
  render() {
    return (
      // Use Browser Router to route to different pages
      <ApolloProvider client={client}>
      <div>
        <BrowserRouter>
          <Main />
        </BrowserRouter>
      </div>
  </ApolloProvider>
    );
  }
}
// Export the App component so that it can be used in index.js
export default App;
