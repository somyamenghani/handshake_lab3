import { gql } from 'apollo-boost';

const getJobPostQuery = gql`
    query($companyId: String){
        jobs(companyId: $companyId) {
            _id
            jobtitle
            postingdate
            deadline
            city
            state
            salary
            description
            jobcategory
            companyname
        }
    }
`;

const getCompanyDetailsQuery = gql`
    query($companyId: String){
        company(companyId: $companyId) {
            name,
            emailId,
            location,
            description
        }
    }
`;
const getStudentDetailsQuery = gql`
    query($studentId: String){
        student(studentId: $studentId) {
            id,
        name,
    password,
    emailId,
    collegeName,
    major,
    careerObjective,
    contactNumber,
    skills,
    educationalDetails
    {
      collegeName  
        major
        degree
        cgpa
        location
        yop
    },
    experienceDetails{
companyName
title
location
startDate
endDate
workDescription
    }
        }
    }
`;
const getJobPostStudentQuery = gql`
    query($input: String){
        jobsAll(input:$input){
            _id
            companyId
            jobtitle
            postingdate
            deadline
            city
            state
            salary
            description
            jobcategory
            companyname
        }
    }
`;

const getAppliedJobQuery = gql`
    query($studentid: String){
        applications(studentid: $studentid) {
            _id
            jobtitle
           applicationdate
        }
    }
`;

const getStudentsQuery = gql`
    query{
        students {
            id,
        name,
    password,
    emailId,
    collegeName,
    major,
    careerObjective,
    contactNumber,
    skills  
        }
    }
`;

const getStudentSearch = gql`
    query($input: String){
        studentSearch(input:$input) {
            id,
        name,
    password,
    emailId,
    collegeName,
    major,
    careerObjective,
    contactNumber,
    skills  
        }
    }
`;
export { getJobPostQuery ,getCompanyDetailsQuery,getJobPostStudentQuery,getStudentDetailsQuery,getAppliedJobQuery,getStudentSearch,getStudentsQuery};