import { gql } from 'apollo-boost';

const addStudentMutation = gql`
    mutation AddStudent($name: String, $emailId: String, $password: String, $collegeName: String, $major: String){

        addStudent(name: $name, emailId: $emailId, password: $password, collegeName: $collegeName, major: $major){
            message
            status
        }
    }
`;
const addCompanyMutation = gql`
    mutation AddCompany($name: String, $emailId: String, $password: String, $location: String){

        addCompany(name: $name, emailId: $emailId, password: $password, location: $location){
            message
            status
        }
    }
`;
const updateCompanyMutation = gql`
    mutation updateCompany($companyId:String,$companyname: String, $emailId: String, $location: String,$description:String){

        updateCompany(companyId:$companyId,companyname: $companyname, emailId: $emailId, location: $location, description:$description){
            message
            status
        }
    }
`;
const loginMutation = gql`
    mutation login($emailId: String, $password: String,$userType:String){
        login(emailId: $emailId, password: $password, userType:$userType){
            message
            status
        }
    }
`;
const addJobPostMutation = gql`
    mutation addJob($userType: String, $companyId:String,$companyname:String,$jobtitle:String,$description:String,$salary:String,$postingdate:String,$deadline:String,$state:String,$city:String,$jobcategory:String){
        addJob(userType:$userType,companyId:$companyId,companyname:$companyname,jobtitle:$jobtitle,description:$description,salary:$salary,postingdate:$postingdate,deadline:$deadline,state:$state,city:$city,jobcategory:$jobcategory){
            message
            status
        }
    }
`;
const searchJobPostMutation = gql`
    mutation searchJob($searchString: String){
        searchJob(searchString:$searchString){
            message
            status
        }
    }
`;
const updateStudentMutation = gql`
    mutation updateStudent($studentId:String,$name: String, $emailId: String,$careerObjective:String,$skills:String){

        updateStudent(studentId:$studentId,name: $name, emailId: $emailId,careerObjective:$careerObjective,skills:$skills){
            message
            status
        }
    }
`;
const applyJobMutation = gql`
    mutation applyJob($studentid:String,$studentname:String,$jobtitle:String,$jobid:String){
        applyJob(studentid:$studentid,studentname:$studentname,jobtitle:$jobtitle,jobid:$jobid){
            message
            status
        }
    }
`;

const getCompanyDetailsMutation = gql`
    mutation company($companyId: String){
        company(companyId: $companyId) {
            name,
            emailId,
            location,
            description
        }
    }
`;
const getStudentDetailsMutation = gql`
    mutation student($studentid: String){
        student(studentid: $studentid) {
            id,
            name,
        password,
        emailId,
        collegeName,
        major,
        careerObjective,
        contactNumber,
        skills ,
        educationalDetails
    {
      collegeName  
        major
        degree
        cgpa
        location
        yop
    },
    experienceDetails{
companyName
title
location
startDate
endDate
workDescription
    }
        }
    }
`;

const getStudentAppliedMutation = gql`
    mutation students($jobid: String){
        students(jobid: $jobid) {
            studentname,
            studentid
            
        }
    }
`;

const addEducationMutation = gql`
    mutation addEducation($userId:String,$userCollege:String,$userMajor:String,$userDegree:String,$userCgpa:String,$userCollegeLocation:String,$userYOP:String){
        addEducation(userId:$userId,userCollege:$userCollege,userMajor:$userMajor,userDegree:$userDegree,userCgpa:$userCgpa,userCollegeLocation:$userCollegeLocation,userYOP:$userYOP){
            message
            status
        }
    }
`;

const addExperienceMutation = gql`
    mutation addExperience($userId:String,$companyName:String,$title:String,$location:String,$startDate:String,$endDate:String,$workDescription:String){
        addExperience(userId:$userId,companyName:$companyName,title:$title,location:$location,startDate:$startDate,endDate:$endDate,workDescription:$workDescription){
            message
            status
        }
    }
`;
export {addStudentMutation,addCompanyMutation,loginMutation,addJobPostMutation,updateCompanyMutation,searchJobPostMutation,updateStudentMutation,applyJobMutation,getCompanyDetailsMutation,getStudentAppliedMutation,getStudentDetailsMutation,addEducationMutation,addExperienceMutation};