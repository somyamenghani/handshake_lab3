const initialState = {
  userId: 0,
  userName: '',
  userEmail: '',
  userType: 1,
};

const reducer = (state = initialState, action) => {
  if (action.type === 'LOADUSER') {
    return {
      userId: localStorage.getItem('user_id'),
      userName: localStorage.getItem('user_name'),
      userEmail: localStorage.getItem('email_id'),
      userType: localStorage.getItem('user_type'),
    };
  }
  if (action.type === 'UNLOADUSER') {
    return state;
  }
  if (action.type === 'USERDETAILS') {
    return {
      userId: localStorage.getItem('user_id'),
      userName: localStorage.getItem('user_name'),
      userEmail: localStorage.getItem('email_id'),
      userType: localStorage.getItem('user_type'),
      user: localStorage.getItem('user_profile'),
    };
  }
  if (action.type === 'COMPANYDETAILS') {
    return {
      userId: localStorage.getItem('user_id'),
      userName: localStorage.getItem('user_name'),
      userEmail: localStorage.getItem('email_id'),
      userType: localStorage.getItem('user_type'),
      user: localStorage.getItem('user_profile'),
    };
  }
  if (action.type === 'STUDENTDASHBOARD') {
    return {
      jobs: localStorage.getItem('jobs'),
    };
  }
  if (action.type === 'COMPANYDASHBOARD') {
    return {
      jobs: localStorage.getItem('jobs'),
    };
  }
  if (action.type === 'STUDENTSFORCOMPANY') {
    return {
      students: localStorage.getItem('students'),
    };
  }
  if (action.type === 'EVENTDETAILS') {
    return {
      events: localStorage.getItem('events'),
    };
  }
  if (action.type === 'EVENTSTUDENTDETAILS') {
    return {
      events: localStorage.getItem('events'),
    };
  }
  if (action.type === 'APPLICATIONS') {
    return {
      applications: localStorage.getItem('applications'),
    };
  }
  if (action.type === 'STUDENTS') {
    return {
      students: localStorage.getItem('students'),
    };
  }
  if (action.type === 'CONVERSATION') {
    return {
      sender: localStorage.getItem('sender'),
      receiver: localStorage.getItem('receiver')
    };
  }
  if (action.type === 'MESSAGES') {
    return {
      conversation: localStorage.getItem('conversation'),
      
    };
  }
  if (action.type === 'SMESSAGES') {
    return {
      conversation: localStorage.getItem('sconversation'),
      
    };
  }
  if (action.type === 'APPLIEDCOMPANY') {
    return {
      appliedCompany:localStorage.getItem('appliedcompany')
    };
  }
  if (action.type === 'APPLIEDJOB') {
    return {
      appliedJob:localStorage.getItem('appliedjob')
    };
  }
  if (action.type === 'SIGNUP') {
    return {
      signupFlag: 'success',
    };
  }

  return state;
};

export default reducer;
