import React, {Component} from 'react';
import '../../App.css';
import axios from 'axios';
import {Redirect} from 'react-router';
import {connect} from 'react-redux';
import { Button } from 'react-bootstrap';
import Modal from 'react-modal';
import dummy from '../../common/dummy.png';
import {backendURI} from '../../common/config';
import { graphql} from 'react-apollo';
import {getStudentsQuery } from '../../queries/queries';
import {getStudentDetailsMutation } from '../../mutation/mutation';
import * as compose from 'lodash.flowright';
class Students extends Component {
    constructor(props) {
        super(props);
        this.state = {
            successUpdate: false,
            students:[],
            searchstring:'',
            student_profile:[],
            educationalDetails:[],
            experienceDetails:[],
            majorFilter:'all',
            pageIndex:1  ,
            count:0,
            openMessage:false   
        };
        this.openStudent = this.openStudent.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.searchStudent=this.searchStudent.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
    }
    componentDidMount(){
        let students=[];
      students=this.props.getStudentsQuery;
        
    }
  
    pageCountInc=async()=>{
        console.log(this.state.students.length);
        if((this.state.students.length)==5)
        {
       await this.setState({
        
            pageIndex:this.state.pageIndex+1 
            
        })
    }
    this.searchStudent();
    }
    pageCountDec=async()=>{
        if(this.state.pageIndex>1)
        {
        await this.setState({
            pageIndex:this.state.pageIndex-1 
        })
      
    }
    this.searchStudent();
    }
    sendMessage(student_profile) {
        this.setState({
            openMessage:true,
            receiverid:student_profile.userId,
            receivername:student_profile.name
        });
    }
    submitMessage=()=>
    {
        let data = {
            userid:localStorage.getItem("user_id"),
            senderid:localStorage.getItem("user_id"),
            sname:localStorage.getItem("user_name"),
            senderUserType:1 ,
            receiverid:this.state.receiverid,
            receivername:this.state.receivername,
            receiverUserType:1,
            content:this.state.message
        }
    axios.post(backendURI +'/messages/',data)
        .then(response => {
            console.log("Status Code : ",response.status,response.data);
            if(response.status === 200){
                alert("Message sent");
                this.searchStudent();
            }
        })
        .catch(err => { 
            this.setState({errorMessage:"Message could no be sent"});
        });
        this.setState({
            openMessage : false
        })
    }
   
    studentCriteria=(e)=>
    {
        this.setState({
            searchstring : e.target.value,
            pageIndex:1
        })
    }
    handleStatusChange=(e)=>{

        this.setState({
            majorFilter: e.target.value
        })
        console.log(this.state.majorFilter)
        this.searchStudent();
    }
    handleSE =async()=>{
        this.state.majorFilter='software engineering'
       await this.setState({
            majorFilter:this.state.majorFilter,
            pageIndex:1
            
        })
          this.searchStudent();
    }
    handleCE=async()=>{
        this.state.majorFilter='computer engineering'
        await this.setState({
            majorFilter:this.state.majorFilter,
            pageIndex:1
            
        })
          this.searchStudent();
    }
    handleBE=async()=>{
        this.state.majorFilter='biomedical engineering'
        await this.setState({
            majorFilter:this.state.majorFilter,
            pageIndex:1
            
        })
          this.searchStudent();
    }
    handleEE=async()=>{
        this.state.majorFilter='electrical engineering'
        await this.setState({
            majorFilter:this.state.majorFilter,
            pageIndex:1
            
        })
          this.searchStudent();
    }
    handleIE=async()=>{
        this.state.majorFilter='industrial engineering'
        await this.setState({
            majorFilter:this.state.majorFilter,
            pageIndex:1
            
        })
          this.searchStudent();
    }
    handleCHE=async()=>{
        this.state.majorFilter='chemical engineering'
        await this.setState({
            majorFilter:this.state.majorFilter,
            pageIndex:1
            
        })
          this.searchStudent();
    }
    handleAll=()=>{
        this.state.majorFilter='all'
        this.setState({
            majorFilter:this.state.majorFilter
            
        })
          this.searchStudent();
    }
    messageContentHandler=(e)=>
    {
        this.setState({
            message : e.target.value
        })
    }

    searchStudent=async(e)=>
    {
        let data = {
            searchString:this.state.searchstring,
            majorFilter:this.state.majorFilter ,
            pageIndex:this.state.pageIndex 
        }
        axios.post(backendURI + '/profile/searchStudents',data)
        .then(response => {
            console.log("Status Code : ",response.status);
            if(response.status === 200){
                let students=response.data;
                console.log(JSON.stringify(students))
               this.setState({
                    students 
                   
                });
                localStorage.setItem("students",JSON.stringify(students));
                this.props.getStudents();
            }
        })
        .catch(err => { 
            this.setState({errorMessage:"Students cannot be viewed"});
        });
    }
   async openStudent(student){
        this.setState({
            openStudent: true ,
            studentid:student._id   
        });
        let mutationResponse = await this.props.getStudentDetailsMutation({
            variables: {
                studentid:student.id
                }, 
    });
    let student_profile = mutationResponse.data.student;
    console.log(student_profile)
    let educationalDetails=mutationResponse.data.student.educationalDetails;
    let experienceDetails=mutationResponse.data.student.experienceDetails;
    this.setState({student_profile,educationalDetails,experienceDetails});
    
    console.log("line146",student_profile)

    }
    closeModal() {
        this.setState({
            openStudent:false
        });
    }
   
    render() 
    { let redirectVar;let studentlist;let filter;let students=[];
    if (!localStorage.getItem("token")) {
        redirectVar = <Redirect to="/login" />;
    }
    if(this.props.data.students)
    {
        students = this.props.data.students;    
    }  
    console.log("students",students)
    let userImage=this.state.student_profile.image||dummy;
    filter=(
    <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Major : {this.state.majorFilter}
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
        <li><a onClick={() => this.handleAll()}>All</a></li>
      <li><a onClick={() => this.handleSE()}>Software Engineering</a></li>
      <li><a onClick={() => this.handleEE()}>Electrical Engineering</a></li>
      <li><a onClick={() => this.handleCE()}>Computer Engineering</a></li>
      <li><a onClick={() => this.handleIE()}>Industrial Engineering</a></li>
      <li><a onClick={() => this.handleCHE()}>Chemical Engineering</a></li>
      <li><a onClick={() => this.handleBE()}>Biomedical Engineering</a></li>
    </ul>
  </div>)
    
    if(students&&students.length>0){
    studentlist =( <div className="panel panel-default p50 uth-panel">
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Student Name</th>
                        <th>College Name</th>
                        <th>Major:
                        </th>
                    </tr>
                </thead>
                <tbody>
                {students.map(student =>
                    <tr key={student._id}>
                    <td>{student.name}</td>
                    <td>{student.collegeName}</td>
                    <td>{student.major}</td>
                    <td><a onClick={() => this.openStudent(student)}>View Student Details</a></td>
                    </tr>
                
                )}
                 </tbody>
            </table>
           
            </div>)
    }
   
    return (
        <div>
        {redirectVar}
        
        <div className="container">     
                    <div className="main-div">
                        <div className="panel">
                            <h2>Student Search</h2>
                              
<div class="row">    
    <div class="col-xs-8 col-xs-offset-2">
        <div>
            <div class="input-group-btn search-panel">
            </div>
            <input type="text" class="form-control" name="search" placeholder="Student Name or College Name" onChange={this.studentCriteria}/>
            <div style={{display: "flex",justifyContent: "center",alignItems: "center"}}>
                <button class="btn btn-primary" type="button" onClick={this.searchStudent}><span class="glyphicon glyphicon-search"></span>Search</button>
            </div>
           
        </div>
    </div>
</div>

</div>
</div>
{studentlist}

<Modal
                            isOpen={this.state.openStudent}
                            onRequestClose={this.closeModal}
                             contentLabel="Example Modal" >
                                 <div className="row mt-3">
                  <div className="col-sm-4">
                      <div className="card" style={{width: 15 +"rem"}}>
                          <img className="card-img-top" src={userImage} alt="" />
                          <div className="text-center">
                          <div className="card-body">
                          <div class="panel panel-default">
                    <div class="panel-heading">Contact</div>
                    <div class="panel-body">{this.state.student_profile.emailId}</div>
                    <div class="panel-body">{this.state.student_profile.contactNumber}</div>
                    <div class="panel-body">{this.state.student_profile.city}</div>
                    <div class="panel-body">{this.state.student_profile.state}</div>
                    <div class="panel-body">{this.state.student_profile.country}</div>
                    </div>
                       </div>
                      </div>
                      </div>
                  </div>
                  
                  <div className="col-sm-7">
                  <div class="panel panel-default">
                    <div class="panel-heading">About</div>
                    <div class="panel-body">{this.state.student_profile.name}</div>
                    <div class="panel-body">{this.state.student_profile.collegeName}</div>
                    <div class="panel-body">{this.state.student_profile.major}</div>

                </div>
                    
                    <div class="panel panel-default">
                    <div class="panel-heading">My Journey</div>
                 <div class="panel-body">{this.state.student_profile.careerObjective}</div>
                </div>
                {this.state.educationalDetails.map(education =>
                    <div class="panel panel-default">
                    <div class="panel-heading">Educational Details</div>
                    <div key={education._id}>
                    <div class="panel-body">College Name: {education.collegeName}</div>
                    <div class="panel-body">College Location: {education.location}</div>
                    <div class="panel-body">Major: {education.major}</div>
                    <div class="panel-body">Degree: {education.degree}</div>
                    <div class="panel-body">CGPA: {education.cgpa}</div>
                    <div class="panel-body">Year of passing:{education.yop}</div>
                    </div>
                </div>
                    )
                    }
                    {this.state.experienceDetails.map(experience =>
                <div class="panel panel-default">
                    <div class="panel-heading">Experience Details</div>
                    <div key={experience._id}>
                    <div class="panel-body">Company Name: {experience.companyName}</div>
                    <div class="panel-body">Title: {experience.title}</div>
                    <div class="panel-body">Location: {experience.location}</div>
                    <div class="panel-body">Start Date: {experience.startDate}</div>
                    <div class="panel-body">End Date: {experience.endDate}</div>
                    <div class="panel-body">Work Description:{experience.workDescription}</div>
                    </div>
                    </div> 
                    )}
                 
                <div class="panel panel-default">
                    <div class="panel-heading">Skills</div>
                     <div class="panel-body">{this.state.student_profile.skills}</div>
                    
                </div>
                      
                  </div>
              </div>     
                         <center>
                         {/* <Button variant="primary" onClick={() => this.sendMessage(this.state.student_profile)}>
                                    <b>Send Message</b>
                                </Button>{" "} */}
                                <Button variant="primary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                        </Modal>
                        <Modal
                            isOpen={this.state.openMessage}
                            onRequestClose={this.cancelModal}
                             contentLabel="Example Modal" >
                                 
                 <form onSubmit={()=>this.submitMessage()}>              
                <div class="panel panel-default">
                    <div class="panel-heading">Message: </div>
                     <div class="panel-body">
                     <div className="input-group mb-2">
    <div className="input-group-prepend">
    </div>
    <input type="text" size="50" name="message" className="form-control" aria-label="description" aria-describedby="basic-addon1" onChange={this.messageContentHandler} 
     pattern=".*\S.*" required />
</div>
                     </div>
                </div>
                         <center>
                         <Button variant="primary" type="submit">
                                    <b>Send</b>
                                </Button>{" "}
                                <Button variant="primary" onClick={this.cancelModal}>
                                    <b>Cancel</b>
                                </Button>
                               
                            </center>
                            </form>
                        </Modal>
</div>
</div>
       
        
)
}
}



export default compose(graphql(getStudentsQuery),

graphql(getStudentDetailsMutation, { name: "getStudentDetailsMutation" }))
(Students);