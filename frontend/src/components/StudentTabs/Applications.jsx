import React, {Component} from 'react';
import '../../App.css';
import axios from 'axios';
import {Redirect} from 'react-router';
import {connect} from 'react-redux';
import { Button } from 'react-bootstrap';
import Modal from 'react-modal';
import {backendURI} from '../../common/config';
import { graphql} from 'react-apollo';
import {getAppliedJobQuery} from '../../queries/queries';
import * as compose from 'lodash.flowright';
class Applications extends Component {
    constructor(props) {
        super(props);
        this.state = {
            applications:[],
            applicationstatus:'all',
            pageIndex:1
                
        };
       
    }
    componentDidMount(){
        let applications;
        applications=this.props.getAppliedJobQuery;
        
    }
   


    render() {
        let redirectVar;let applicationlist;let applications=[];
        if (!localStorage.getItem("token")) {
            redirectVar = <Redirect to="/login" />;
        }
       
        if(this.props.data.applications)
        {
            applications=this.props.data.applications;
           
           console.log("line138",this.props.data.applications)  
            
        }   
        return (
            <div>
            {redirectVar}
            <div className="container">
        <div className="panel panel-default p50 uth-panel">
        <table className="table table-hover">
            <thead>
                <tr>
                    <th>Job Title</th>
                    <th>Application Date</th>
                    
                    
                </tr>
            </thead>
            <tbody>
            {applications.map(application =>
                <tr>
                <td>{application.jobtitle} </td>
            <td>{application.applicationdate}</td>
           
                </tr>
            )}
             </tbody>
        </table>
       
        </div>
        </div>
            </div>
           
            
    )
  }
}



export default compose(
   
    graphql(getAppliedJobQuery, {
        options: {
            variables: { studentid: localStorage.getItem("user_id") }
        }
    })
   
)(Applications);