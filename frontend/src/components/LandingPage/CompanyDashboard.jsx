import React, {Component} from 'react';
import '../../App.css';
import axios from 'axios';
import {Redirect} from 'react-router';
import {connect} from 'react-redux';
import { Button } from 'react-bootstrap';
import Modal from 'react-modal';
import dummy from '../../common/dummy.png';
import {backendURI} from '../../common/config';
import { graphql} from 'react-apollo';
import {addJobPostMutation } from '../../mutation/mutation';
import {getStudentAppliedMutation } from '../../mutation/mutation';
import {getStudentDetailsMutation } from '../../mutation/mutation';
import {getJobPostQuery } from '../../queries/queries';
import * as compose from 'lodash.flowright';

class CompanyDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            jobs: []  ,
            students:[],
        modalIsOpen: false, 
        addIsOpen:false,
        openStatus:false,
        openStudent:false,
        openResume:false,
        jobtitle: '',
        description: '',
        msg: '',
        jobid: 0 ,
        categoryValue:'full-time',
        applicationStatus:'pending',
        studentid:'',
        student_profile:[],
        educationalDetails:[],
        experienceDetails:[],
        pageIndex:1  
        };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.closeStudentModal = this.closeStudentModal.bind(this);
        this.addJobPost = this.addJobPost.bind(this);
        this.openStatus = this.openStatus.bind(this);
        this.openStudent = this.openStudent.bind(this);
        this.openResume = this.openResume.bind(this);
        this.closeResumeModal = this.closeResumeModal.bind(this);
    }
    componentDidMount(){
        let jobs=[];
      jobs=this.props.getJobPostQuery;
    }
   

    onAddSubmit=async(e)=>
    {
        e.preventDefault();
        let mutationResponse = await this.props.addJobPostMutation({
            variables: {
                userType:localStorage.getItem("user_type"),
                companyId:localStorage.getItem("user_id"),
                companyname:localStorage.getItem("user_name"),
                jobtitle:this.state.jobtitle,
                description: this.state.description,
                salary: this.state.salary,
                postingdate:this.state.postingdate,
                deadline:this.state.deadline,
                state:this.state.state,
                city:this.state.city,
                jobcategory:this.state.categoryValue
            },
            refetchQueries: [{ query: getJobPostQuery ,
                variables: { companyId: localStorage.getItem("user_id") }}]
        
        });
        let response = mutationResponse.data.addJob;
        console.log(response)
            if(response.status === 200){
                this.setState({
                    addIsOpen : false
                })
                alert("Job Added")
            }
    }
    async openModal(job) {
        this.setState({
            modalIsOpen: true,
                  
        });
        const data={jobid :job._id};

        let mutationResponse = await this.props.getStudentAppliedMutation({
            variables: {
            jobid:job._id
        }
    });
    let students = mutationResponse.data.students;
    this.setState({
        students
              
    });



      

    }
   async openStudent(student){
        this.setState({
            openStudent: true ,
            studentid:student.studentid   
        });

        let mutationResponse = await this.props.getStudentDetailsMutation({
            variables: {
                studentid:student.studentid
                }, 
    });
    let student_profile = mutationResponse.data.student;
    let educationalDetails=mutationResponse.data.student.educationalDetails;
    let experienceDetails=mutationResponse.data.student.experienceDetails;
    this.setState({student_profile,educationalDetails,experienceDetails});
    
    console.log("line146",student_profile)
    }
    openStatus(student) {
        this.setState({
            openStatus: true ,
            studentid:student.studentid,
            jobid:student.jobid      
        });
    }
    onUpdateStatus=(e)=>{
        e.preventDefault()
        const data={studentid :this.state.studentid,
                    jobid:this.state.jobid,
                    status:this.state.applicationStatus      
        };
        axios.post(backendURI +'/jobs/updateApplicationStatus',data)
        .then(response => {
            console.log("Status Code : ",response.status);
            if(response.status === 200){
                alert("updated status")
                // let students=response.data;
                // console.log(JSON.stringify(students))
                this.setState({
                    openStatus:false 
                });
            }
        })
        .catch(err => { 
            this.setState({errorMessage:"Student list could not be viewed"});
        });

    }
    openStatus(student) {
        this.setState({
            openStatus: true ,
            studentid:student.studentid,
            jobid:student.jobid      
        });
    }
    openResume(student) {
        this.setState({
            openResume: true ,
            studentid:student.studentid,
            jobid:student.jobid,
            resume:student.resume
        });
    }
    pageCountInc=async()=>{
        console.log(this.state.jobs.length);
        if((this.state.jobs.length)==5)
        {
       await this.setState({
        
            pageIndex:this.state.pageIndex+1 
            
        })
    }
   // this.viewJobPosting();
    }
    pageCountDec=async()=>{
        if(this.state.pageIndex>1)
        {
        await this.setState({
            pageIndex:this.state.pageIndex-1 
        }) 
    }
   // this.viewJobPosting();
}
sendMessage(student_profile) {
    this.setState({
        openMessage:true,
        receiverid:student_profile.userId,
        receivername:student_profile.name
    });
}

submitMessage=()=>
{
    let data = {
        senderid:localStorage.getItem("user_id"),
        sname:localStorage.getItem("user_name"),
        senderUserType:2 ,
        receiverid:this.state.receiverid,
        receivername:this.state.receivername,
        receiverUserType:1,
        content:this.state.message
    }
axios.post(backendURI +'/messages/',data)
    .then(response => {
        console.log("Status Code : ",response.status,response.data);
        if(response.status === 200){
            alert("Message sent");
            this.searchStudent();
        }
    })
    .catch(err => { 
        this.setState({errorMessage:"Message could no be sent"});
    });
    this.setState({
        openMessage : false
    })
}

messageContentHandler=(e)=>
    {
        this.setState({
            message : e.target.value
        })
    }

    
    handleStatusChange=(e)=>{

        this.setState({
            applicationStatus: e.target.value
        })
    }
    jobTitleChange=(e)=>{
        this.setState({
            jobtitle : e.target.value
        })
    }
    descriptionChange = (e) => {
        this.setState({
            description : e.target.value
        })
    }
    handleCategoryChange = (e) => {
        this.setState({
            categoryValue : e.target.value
        })
    }
    handleSalaryChange = (e) => {
        this.setState({
            salary : e.target.value
        })
    }
    handleCityInfoChange = (e) => {
        this.setState({
            city: e.target.value
        })
    }
    handleStateInfoChange = (e) => {
        this.setState({
            state: e.target.value
        })
    }
    handleDeadlineChange = (e) => {
        this.setState({
            deadline: e.target.value
        })
    }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            addIsOpen: false
            
        });
    }
    closeStudentModal() {
        this.setState({
            openStatus:false,
            openStudent:false
        });
    }
    closeResumeModal() {
        this.setState({
            openResume:false
        });
    }
    
    addJobPost(){

        this.setState({
            addIsOpen: true,          
        });
    }
    cancelModal() {
        this.setState({
            openMessage:false
        });
    }
   
   
    render() {
        let redirectVar;let jobs=[];
        if (!localStorage.getItem("token")) {
            redirectVar = <Redirect to="/login" />;
        }
        let userImage=this.state.student_profile.image||dummy;

        if(this.state.jobs.length==0)
        {
            this.pageCountDec();
        }
       if(this.props.data.jobs)
{
    jobs = this.props.data.jobs;    
}       console.log(this.props.data.jobs)
        return (
            <div>
            {redirectVar}
            <div className="container"> 
            <div className="panel panel-default p50 uth-panel">
            <div class="panel-heading clearfix">
            <button type="button" class="btn btn-primary btn-block pull-right" onClick={this.addJobPost}>Add job post</button>
        </div>
        
            
                <table className="table table-hover">
                    <thead>
                        <tr>
                            
                            <th>Job Title</th>
                            <th>Job Description</th>
                            <th>Job Category</th>
                            <th>Job Salary</th>
                            <th>Job Location</th>
                            <th>Posting Date</th>
                            <th>Deadline</th>
                            
                            
                        </tr>
                    </thead>
                    <tbody>
                        
                    {jobs.map(job =>
                        <tr key={job.jobid}>
                       
                        <td>{job.jobtitle} </td>
                        <td>{job.description}</td>
                        <td>{job.jobcategory}</td>
                        <td>{job.salary}</td>
                    <td>{job.city} {job.state}</td>
                    <td>{job.postingdate}</td>
                    <td>{job.deadline}</td>

                        <td><a onClick={() => this.openModal(job)}>View Students Applied</a></td>
                        </tr>
                    )}
                     </tbody>
                </table>
              

                        <Modal
                            isOpen={this.state.modalIsOpen}
                            onRequestClose={this.closeModal}
                             contentLabel="Example Modal" >
                           
                           <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Student Name</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.students.map(student =>
                        <tr key={student.studentid}>
                        <td>{student.studentname} </td>
                        <td><a onClick={() => this.openStudent(student)}>View Profile</a></td>
                        
                        </tr>
                    )}
                     </tbody>
                </table>
                            <center>
                                <Button variant="primary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                        </Modal>

                        <Modal
                            isOpen={this.state.openStatus}
                            onRequestClose={this.closeStudentModal}
                             contentLabel="Example Modal" >
                            
                         <form onSubmit={this.onUpdateStatus}>
                         <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Application Status</b></span>
                                </div>
                                <select value={this.state.applicationStatus} onChange={this.handleStatusChange}  className="form-control" aria-label="category" aria-describedby="basic-addon1"  required >
                                <option value="pending">Pending</option>
                                <option value="reviewed">Reviewed</option>
                                <option value="declined">Declined</option>
                                </select>
                            </div>
                            <center>
                            <Button variant="primary" type="submit">
                                    <b>Update Status</b>
                                    </Button>
                                <Button variant="primary" onClick={this.closeStudentModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                            </form>
                    
                        </Modal>
                        <Modal
                            isOpen={this.state.openResume}
                            onRequestClose={this.closeResumeModal}
                             contentLabel="Example Modal" >
                         <div>   
                  <div class="panel panel-default">
                    <div class="panel-heading">Resume</div>
                    <p><a href={this.state.resume}>Download Resume</a></p>
                </div> 
                            <center>
                                <Button variant="primary" onClick={this.closeResumeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                            </div>
                    
                        </Modal>
                        <Modal
                            isOpen={this.state.openStudent}
                            onRequestClose={this.closeStudentModal}
                             contentLabel="Example Modal" >
                               <div className="row mt-3">
                  <div className="col-sm-4">
                      <div className="card" style={{width: 15 +"rem"}}>
                          <img className="card-img-top" src={userImage} alt="" />
                          <div className="text-center">
                          <div className="card-body">
                          <div class="panel panel-default">
                    <div class="panel-heading">Contact</div>
                    <div class="panel-body">{this.state.student_profile.emailId}</div>
                    <div class="panel-body">{this.state.student_profile.contactNumber}</div>
                    <div class="panel-body">{this.state.student_profile.city}</div>
                    <div class="panel-body">{this.state.student_profile.state}</div>
                    <div class="panel-body">{this.state.student_profile.country}</div>
                    </div>
                       </div>
                      </div>
                      </div>
                  </div>
                  
                  <div className="col-sm-7">
                  <div class="panel panel-default">
                    <div class="panel-heading">About</div>
                    <div class="panel-body">{this.state.student_profile.name}</div>
                    <div class="panel-body">{this.state.student_profile.collegeName}</div>
                    <div class="panel-body">{this.state.student_profile.major}</div>

                </div>
                    
                    <div class="panel panel-default">
                    <div class="panel-heading">My Journey</div>
                 <div class="panel-body">{this.state.student_profile.careerObjective}</div>
                </div>
                {this.state.educationalDetails.map(education =>
                    <div class="panel panel-default">
                    <div class="panel-heading">Educational Details</div>
                    <div key={education._id}>
                    <div class="panel-body">College Name: {education.collegeName}</div>
                    <div class="panel-body">College Location: {education.location}</div>
                    <div class="panel-body">Major: {education.major}</div>
                    <div class="panel-body">Degree: {education.degree}</div>
                    <div class="panel-body">CGPA: {education.cgpa}</div>
                    <div class="panel-body">Year of passing:{education.yop}</div>
                    </div>
                </div>
                    )
                    }
                    {this.state.experienceDetails.map(experience =>
                <div class="panel panel-default">
                    <div class="panel-heading">Experience Details</div>
                    <div key={experience._id}>
                    <div class="panel-body">Company Name: {experience.companyName}</div>
                    <div class="panel-body">Title: {experience.title}</div>
                    <div class="panel-body">Location: {experience.location}</div>
                    <div class="panel-body">Start Date: {experience.startDate}</div>
                    <div class="panel-body">End Date: {experience.endDate}</div>
                    <div class="panel-body">Work Description:{experience.workDescription}</div>
                    </div>
                    </div> 
                    )}
                 
                <div class="panel panel-default">
                    <div class="panel-heading">Skills</div>
                     <div class="panel-body">{this.state.student_profile.skills}</div>
                    
                </div>
                      
                  </div>
              </div>     
                         <center>
                         {/* <Button variant="primary" onClick={() => this.sendMessage(this.state.student_profile)}>
                                    <b>Send Message</b>
                                </Button>{" "} */}
                                <Button variant="primary" onClick={this.closeStudentModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                        </Modal>
                   
                   
                <Modal
                            isOpen={this.state.addIsOpen}
                            onRequestClose={this.closeAddModal}
                             contentLabel="Example Modal" >
                           
                           <div>
                         <form onSubmit={this.onAddSubmit}>
                             
            <div class="container">
            <div class="panel panel-default">
                    <div class="panel-heading">Job Post Details</div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Job Title</b></span>
                                </div>
                                <input type="text" size="50" name="jobTitle" className="form-control" aria-label="jobTitle" aria-describedby="basic-addon1" onChange={this.jobTitleChange}   pattern=".*\S.*" title="Job Title cannot be spaces" required />
                            </div>

                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Job Description</b></span>
                                </div>
                                <input type="text" size="50" name="description" className="form-control" aria-label="description" aria-describedby="basic-addon1" onChange={this.descriptionChange} 
                                 pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Job Category</b></span>
                                </div>
                                <select value={this.state.categoryValue} onChange={this.handleCategoryChange}  className="form-control" aria-label="category" aria-describedby="basic-addon1"  required >
                                <option value="full-time">Full Time</option>
                                <option value="part-time">Part Time</option>
                                <option value="on-campus">On Campus</option>
                                <option value="intern">Intern</option>
                                </select>
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Salary</b></span>
                                </div>
                                <input type="text" size="50"   name="salary" className="form-control" aria-label="salary" aria-describedby="basic-addon1"  onChange={this.handleSalaryChange} pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>City</b></span>
                                </div>
                                <input type="text" size="50" name="city" className="form-control" aria-label="city" aria-describedby="basic-addon1" onChange={this.handleCityInfoChange}   pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>State</b></span>
                                </div>
                                <input type="text" size="50" name="state" className="form-control" aria-label="city" aria-describedby="basic-addon1" onChange={this.handleStateInfoChange}   pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Deadline</b></span>
                                </div>
                                <input type="date" size="50" name="deadline" className="form-control" aria-label="deadline" aria-describedby="basic-addon1" onChange={this.handleDeadlineChange}   pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" placeholder="YYYY-MM-DD" title="Enter a date in this formart YYYY-MM-DD" required />
                            </div>
                            <center>
                                <Button variant="primary" type="submit">
                                    <b>Add</b>
                                </Button>&nbsp;&nbsp;
                                <Button variant="secondary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                            </div>
                            </div>
                        </form>
                        </div>
                       
                        </Modal>
                        <Modal
                            isOpen={this.state.openMessage}
                            onRequestClose={this.cancelModal}
                             contentLabel="Example Modal" >
                                 
                 <form onSubmit={()=>this.submitMessage()}>              
                <div class="panel panel-default">
                    <div class="panel-heading">Message: </div>
                     <div class="panel-body">
                     <div className="input-group mb-2">
    <div className="input-group-prepend">
    </div>
    <input type="text" size="50" name="message" className="form-control" aria-label="description" aria-describedby="basic-addon1" onChange={this.messageContentHandler} 
     pattern=".*\S.*" required />
</div>
                     </div>
                </div>
                         <center>
                         <Button variant="primary" type="submit">
                                    <b>Send</b>
                                </Button>
                                <Button variant="primary" onClick={this.cancelModal}>
                                    <b>Cancel</b>
                                </Button>
                               
                            </center>
                            </form>
                        </Modal>
            </div>
        </div>
        </div>
        );
    }
}


export default compose(
    graphql(addJobPostMutation, { name: "addJobPostMutation" }),
    graphql(getJobPostQuery, {
        options: {
            variables: { companyId: localStorage.getItem("user_id") }
        }
    }
   
),graphql(getStudentAppliedMutation, { name: "getStudentAppliedMutation" }),
graphql(getStudentDetailsMutation, { name: "getStudentDetailsMutation" })
)(CompanyDashboard);