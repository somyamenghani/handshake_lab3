import React, {Component} from 'react';
import '../../App.css';
import axios from 'axios';
import {Redirect} from 'react-router';
import {connect} from 'react-redux';
import { Button } from 'react-bootstrap';
import Modal from 'react-modal';
import dummy from '../../common/dummy.png';
import {backendURI} from '../../common/config';
import { graphql, Query} from 'react-apollo';

import {getJobPostStudentQuery } from '../../queries/queries';
import {getCompanyDetailsQuery} from '../../queries/queries';
import {searchJobPostMutation } from '../../mutation/mutation';
import {getCompanyDetailsMutation } from '../../mutation/mutation';
import {applyJobMutation } from '../../mutation/mutation';
import * as compose from 'lodash.flowright';
import { useQuery } from '@apollo/react-hooks';
let searchString="";
class StudentDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            jobs: []  ,
            modalIsOpen: false, 
            addIsOpen:false,
            companyIsOpen:false,
            applyIsOpen:false,
            jobtitle: '',
            description: '',
            msg: '',
            location:'',
            searchstring:'',
            
            categoryValue:'all',
            successUpdate: false,
            company_profile:[],
            jobid:'',
            pageIndex:1,
            cityFilter:'all',
            sortOrder:'deadline,asc',
            pageIndex:1,
            company:{},
            joblist:[]
           
        };
        this.openModal = this.openModal.bind(this);
        this.openCompanyModal = this.openCompanyModal.bind(this);
        this.openApplyModal = this.openApplyModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.jobtitleSearch=this.jobtitleSearch.bind(this);
        this.searchJobPosting=this.searchJobPosting.bind(this);
    }
    componentDidMount(){
        //localStorage.setItem("search",searchString);
        let jobs=[];
         jobs=this.props.getJobPostStudentQuery;
       // this.searchJobPosting();
    }
    
    
    jobtitleSearch=async(e)=>
    {
        await this.setState({
            searchstring : e.target.value
          
        })
    }
    handleCategoryChange=async(e)=>
    {
       await this.setState({
            categoryValue : e.target.value,
            pageIndex:1
        })

    }
    closeModal() {
        this.setState({
            modalIsOpen: false,
            companyIsOpen:false,
            applyIsOpen:false
        });
    }
    onSubmit = async (e) => {
        e.preventDefault();
       
        
        let mutationResponse = await this.props.applyJobMutation({
            variables: {
            studentid:localStorage.getItem("user_id"),
            studentname:localStorage.getItem("user_name"),
                jobid:this.state.jobid,
                jobtitle:this.state.jobtitle
        }
    });
    let response = mutationResponse.data.applyJob;
    if(response.status === 200){
                alert("Applied Successfully");
                this.setState({
                    applyIsOpen:false  
                });
            }
   
    console.log("line100",response)
    }
    openModal(job) {
        this.setState({
            modalIsOpen: true,
            jobtitle:job.jobtitle,
            description:job.description         
        });
    }
    openApplyModal(job) {
        this.setState({
            applyIsOpen: true,
            jobid:job._id,
            jobtitle:job.jobtitle
           
        });
    }
    async openCompanyModal(job) {
        this.setState({
            companyIsOpen: true,
        });
       let companyId=job.companyId;

       let mutationResponse = await this.props.getCompanyDetailsMutation({
        variables: {
            companyId:job.companyId
            }, 
});
let company = mutationResponse.data.company;
this.setState({company});

console.log("line146",company)

        }
       

    searchJobPosting=async(e)=>
    {
        console.log("getting called");
        e.preventDefault();
        let job=[];
        searchString=this.state.searchstring;
        e.preventDefault();
        let mutationResponse = await this.props.searchJobPostMutation({
            variables: {
                
                searchString:this.state.searchstring
            },
            refetchQueries: [{ query: getJobPostStudentQuery ,
                variables: { searchInput: searchString}}]
        
        });
        let joblist = mutationResponse.data.searchJob.message;
        console.log(JSON.parse(joblist))
        this.setState({joblist});
        
    }
    handleResumeChange = (e) => {
        console.log(e.target.files[0])
        this.setState({
            userResume: e.target.files[0]
        });
    };
   
    render() {
        let redirectVar;let joblist;let company,name;let filter;let categoryfilter;let sortOrderFilter;let jobs=[];
        if (!localStorage.getItem("token")) {
            redirectVar = <Redirect to="/login" />;
        }
       

        if(this.props.data.jobsAll)
        {
jobs=this.props.data.jobsAll;
        }
        let userImage=this.state.company_profile.image||dummy;

console.log("line375",this.props.data.jobsAll)
console.log(this.state.joblist)


        if (this.state && this.state.company_profile) {
            company = this.state.company_profile;
            name = company.name;
            }
            joblist =( <div className="panel panel-default p50 uth-panel">
                <table className="table table-hover">
                    <thead>
                        <tr>
                            
                            <th>Company Name</th>
                            <th>Job Title</th>
                            <th>Job Description</th>
                            <th>Job Category</th>
                            <th>Job Salary</th>
                            <th>Job Location</th>
                            <th>Posting Date</th>
                            <th>Deadline</th>   
                        </tr>
                    </thead>
                    <tbody>
                    {jobs.map(job =>
                        <tr key={job._id}>
                        <td><a onClick={() => this.openCompanyModal(job)}>{job.companyname}</a></td>
                        <td>{job.jobtitle} </td>
                        <td><a onClick={() => this.openModal(job)}>View Job Description</a></td>
                        <td>{job.jobcategory}</td>
                        <td>{job.salary}</td>
                    <td>{job.city} {job.state}</td>
                    <td>{job.postingdate}</td>
                    <td>{job.deadline}</td>
                        <td><a onClick={() => this.openApplyModal(job)}>Apply</a></td>
                        </tr>
                    )}
                     </tbody>
                </table>
              
                    <Modal
                            isOpen={this.state.modalIsOpen}
                            onRequestClose={this.closeModal}
                             contentLabel="Example Modal" >
                           <div>             
                            <div class="container">
                            <div class="panel panel-default">
                            <div class="panel-heading">Job Description </div>
                            <div class="panel-heading">{this.state.jobtitle}</div>
                            <div class="panel-body">{this.state.description}</div>
                            <center> 
                                <Button variant="primary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                            </div>
                            </div>
                        </div>
                        </Modal>
                        <Modal
                            isOpen={this.state.companyIsOpen}
                            onRequestClose={this.closeModal}
                             contentLabel="Example Modal" >
                           <div>             
                            <div class="container">
                            <div className="row mt-3">
                      <div className="col-sm-4">
                          <div className="card" style={{width: 15 +"rem"}}>
                              <img className="card-img-top" src={userImage} alt="" />
                              <div className="text-center">
                              <div className="card-body">

                              <div class="panel panel-default">
                        <div class="panel-heading">Contact</div>
                        <a>
                        <i class="glyphicon glyphicon-envelope"></i>
                        <div class="panel-body">{this.state.company.emailId}</div>
                        </a>
                        </div>
                           </div>
                          </div>
                          </div>
                      </div>
                      
                      <div className="col-sm-7">
                      <div class="panel panel-default">
                     
                        <div class="panel-heading">Name</div>
                        <div class="panel-body">{this.state.company.name}</div>
                        
                        
                    </div>
                        
                    <div class="panel panel-default">
                        <div class="panel-heading">Location</div>
                        <div class="panel-body">{this.state.company.location}</div>
                       
                        
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Description</div>
                        <div class="panel-body">{this.state.company.description}</div>
          
                        
                    </div>
                   
                      </div>  
                  </div> 
                            <center> 
                                <Button variant="primary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                            </div>
                            
                        </div>
                        </Modal>
                        <Modal
                            isOpen={this.state.applyIsOpen}
                            onRequestClose={this.closeModal}
                             contentLabel="Example Modal"
                              >
                           
                           <div>
                         <form onSubmit={this.onSubmit}>
                             
            <div class="container">
            <div class="panel panel-default">
    <div class="panel-heading">Apply for job:{this.state.jobtitle}</div>
                    
                            <center>
                                <Button variant="primary" type="submit">
                                    <b>Submit</b>
                                </Button>&nbsp;&nbsp;
                                <Button variant="secondary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                            </div>
                            </div>
                        </form>
                        </div>
                        </Modal>
                    </div>
                    )
       
        return (
            <div>
            {redirectVar}
            
            <div className="container">
                    
                        
                            <div className="panel">
                                <h2>Job Search</h2>
                                  
    <div class="row">    
        <div class="col-xs-8 col-xs-offset-2">
		    <div class="main-div">
                <div class="input-group-btn search-panel">
                    
                </div>
                <form onSubmit={this.searchJobPosting}>
                <input type="text" class="form-control" name="jobtitle" placeholder="Job Title or Company Name" onChange={this.jobtitleSearch}/>
                
                
                <div  style={{display: "flex",justifyContent: "center",alignItems: "center"}} >
                    <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-search"></span>Search</button>
                </div>
                </form>
            </div>
        </div>
        
	</div>
    
</div>

{joblist}
</div>
</div>
           
            
    )
  }
}

export default compose(
    
    graphql(searchJobPostMutation, { name: "searchJobPostMutation" }),
    graphql(getJobPostStudentQuery,{
        options: () => {
            return {
                variables: {
                    input: searchString
                }
            }
        }
    }
   
),
graphql(applyJobMutation, { name: "applyJobMutation" }),
graphql(getCompanyDetailsMutation, { name: "getCompanyDetailsMutation" })

)(StudentDashboard);