import React, {Component} from 'react';
import '../../App.css';
import axios from 'axios';
import {Redirect} from 'react-router';
import {connect} from 'react-redux';
import {Button} from 'react-bootstrap';
import Modal from 'react-modal';
import dummy from '../../common/dummy.png';
import {backendURI} from '../../common/config';

class EventsCompany extends Component {
    constructor(props) {
        super(props);
        this.state = {
            events: [] ,
            students:[],
            successUpdate: false,
            addIsOpen:false  ,
            modalIsOpen: false,
            categoryValue:'all',
            eventName:'',
            description:'',
            time:'',
            date:'',
            location:'',
            city:'',
            updateFlag:false,
            openStudent:false,
            student_profile:[],
            educationalDetails:[],
            experienceDetails:[],
            pageIndex:1

        };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.addEvent = this.addEvent.bind(this);
        this.closeAddModal=this.closeAddModal.bind(this);
        this.closeStudentModal = this.closeStudentModal.bind(this);
    }
    componentDidMount(){
        this.viewEvents();
        
    }

    viewEvents=()=>
    {
        
        let data = {
            userType:localStorage.getItem("user_type"),
            companyId:localStorage.getItem("user_id"),
            pageIndex:this.state.pageIndex
        }
        axios.post(backendURI+'/events/details',data)
        .then(response => {
            console.log("Status Code : ",response.status);
            if(response.status === 200){
                let events=response.data;
                
                this.setState({
                    events
                   
                });
                localStorage.setItem("events",JSON.stringify(events));
                this.props.getEvents();
            }
        })
        .catch(err => { 
            this.setState({errorMessage:"Event could not be added"});
        });


    }

    handleCategoryChange =(e)=>{
        this.setState({
            categoryValue : e.target.value
        })

    }
    eventNameChangeHandler = (e) => {
        this.setState({
            eventName : e.target.value
        })
    }
    descriptionChangeHandler = (e) => {
        this.setState({
            description : e.target.value
        })
    }
    timeChangeHandler = (e) => {
        this.setState({
            time : e.target.value
        })
    }
    dateChangeHandler = (e) => {
        this.setState({
            date : e.target.value
        })
    }
    locationChangeHandler = (e) => {
        this.setState({
            location : e.target.value
        })
    }
    cityChangeHandler = (e) => {
        this.setState({
            city : e.target.value
        })
    }
    onAddSubmit=(e)=>
    {
        e.preventDefault()
        let data = {
            userType:localStorage.getItem("user_type"),
            companyId:localStorage.getItem("user_id"),
            eventName:this.state.eventName,
            description: this.state.description,
            time: this.state.time,
            date:this.state.date,
            location:this.state.location,
            city:this.state.city,
            eligibility:this.state.categoryValue
        }
        axios.post(backendURI+'/events',data)
        .then(response => {
            console.log("Status Code : ",response.status);
            if(response.status === 200){
                this.setState({
                    addIsOpen : false
                })
            }
            this.viewEvents();
        })
        .catch(err => { 
            this.setState({errorMessage:"Event could not be added"});
        });


    }

    openModal(event) {
        this.setState({
            modalIsOpen: true,       
        });
        const data={eventid :event._id};
        axios.post(backendURI + '/events/getStudentApplied',data)
        .then(response => {
            console.log("Status Code : ",response.status);
            if(response.status === 200){
                let students=response.data;
                console.log(JSON.stringify(students))
                this.setState({
                    students 
                });
            }
        })
        .catch(err => { 
            this.setState({errorMessage:"Student list could not be viewed"});
        });

    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        });
    }
    closeAddModal() {
        this.setState({
            addIsOpen: false
        });
    }
    closeStudentModal() {
        this.setState({
            openStudent:false
        });
    }
    addEvent(){

        this.setState({
            addIsOpen: true         
        });
    }
    openStudent(student){
        this.setState({
            openStudent: true ,
            studentid:student.studentid   
        });
        const data={userId :student.studentid};
        axios.post(backendURI + '/profile',data)
        .then(response => {
            console.log("Status Code : ",response.status);
            if(response.status === 200){
                let student_profile=response.data;
                let educationalDetails=response.data.educationalDetails;
        let experienceDetails=response.data.experienceDetails;
                console.log(JSON.stringify(student_profile))
                this.setState({
                    student_profile,educationalDetails,experienceDetails 
                });
            }
        })
        .catch(err => { 
            this.setState({errorMessage:"Student profile profile could not be viewed"});
        });
    }
    pageCountInc=async()=>{
        console.log(this.state.events.length);
        if((this.state.events.length)==5)
        {
       await this.setState({
        
            pageIndex:this.state.pageIndex+1 
            
        })
    }
    this.viewEvents();
    }
    pageCountDec=async()=>{
        if(this.state.pageIndex>1)
        {
        await this.setState({
            pageIndex:this.state.pageIndex-1 
        }) 
    }
    this.viewEvents();
}
   
    render() {
        let redirectVar;
        if (!localStorage.getItem("token")) {
            redirectVar = <Redirect to="/login" />;
        }
        let userImage=this.state.student_profile.image||dummy;
        
       
        return (
            <div>
            {redirectVar}
           <div className="panel panel-default p50 uth-panel">
            <div class="panel-heading clearfix">
            <button type="button" class="btn btn-primary btn-block pull-right" onClick={this.addEvent}>Add Events</button>
             </div>
             <table className="table table-hover">
                    <thead>
                        <tr>
                            
                            <th>Event Name</th>
                            <th>Event Description</th>
                            <th>Eligibility</th>
                            <th>Time</th>
                            <th>Location</th>
                            <th>City</th>
                            <th>Date</th> 
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.events.map(event =>
                        <tr key={event.EventId}>
                       
                        <td>{event.eventname} </td>
                        <td>{event.description}</td>
                        <td>{event.eligibility}</td>
                        <td>{event.time}</td>
                        <td>{event.location}</td>
                        <td>{event.city}</td>
                        <td>{event.date}</td>

                        <td><a onClick={() => this.openModal(event)}>View Students Applied</a></td>
                        </tr>
                    )}
                      <Modal
                            isOpen={this.state.modalIsOpen}
                            onRequestClose={this.closeModal}
                             contentLabel="Example Modal" >       
                     <div>
                     Student Details
                    </div>
                    <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Student Name</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.students.map(student =>
                        <tr key={student.studentid}>
                        <td>{student.studentname} </td>
                        <td><a onClick={() => this.openStudent(student)}>View Profile</a></td>
                        
                        </tr>
                    )}
                     </tbody>
                </table>
                            <center>
                                <Button variant="primary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                       
                        </Modal>
                    </tbody>
                </table>

                <div style={{display: "flex",justifyContent: "center",alignItems: "center"}}>
            <nav aria-label="Page navigation example">
  <ul className="pagination justify-content-center">
    <li class="page-item">
      <a class="page-link" href="#" onClick={() => this.pageCountDec()} tabIndex="-1">Previous</a>
    </li>
                <li class="page-item"><a class="page-link" href="#">{this.state.pageIndex}</a></li>
    <li class="page-item">
      <a class="page-link" href="#" onClick={() => this.pageCountInc()}>Next</a>
    </li>
  </ul>
</nav>
</div>
        
            

<Modal
isOpen={this.state.addIsOpen}
onRequestClose={this.closeAddModal}
 contentLabel="Example Modal" >

<div>
<form onSubmit={this.onAddSubmit}>
 
<div class="container">
<div class="panel panel-default">
<div class="panel-heading">Event Details</div>
<div className="input-group mb-2">
    <div className="input-group-prepend">
        <span className="input-group-text" id="basic-addon1"><b>Event Name</b></span>
    </div>
    <input type="text" size="50" name="eventName" className="form-control" aria-label="eventName" aria-describedby="basic-addon1" onChange={this.eventNameChangeHandler}   pattern=".*\S.*" title="Event name cannot be spaces" required />
</div>

<div className="input-group mb-2">
    <div className="input-group-prepend">
        <span className="input-group-text" id="basic-addon1"><b>Event Description</b></span>
    </div>
    <input type="text" size="50" name="description" className="form-control" aria-label="description" aria-describedby="basic-addon1" onChange={this.descriptionChangeHandler} 
     pattern=".*\S.*" required />
</div>
<div className="input-group mb-2">
    <div className="input-group-prepend">
        <span className="input-group-text" id="basic-addon1"><b>Eligibility</b></span>
    </div>
    <select value={this.state.categoryValue} onChange={this.handleCategoryChange}  className="form-control" aria-label="category" aria-describedby="basic-addon1"  required >
    <option value="all">All</option>
    <option value="Software Engineering">Software Engineering</option>
    <option value="Electrical Engineering">Electrical Engineering</option>
    <option value="Computer Science Engineering">Computer Science Engineering</option>
    <option value="Industrial Engineering">Industrial Engineering</option>
    <option value="Civil Engineering">Civil Engineering</option>
    <option value="Computer Engineering">Computer Engineering</option>
    </select>
</div>
<div className="input-group mb-2">
    <div className="input-group-prepend">
        <span className="input-group-text" id="basic-addon1"><b>Time</b></span>
    </div>
    <input type="text" size="50"   name="time" className="form-control" aria-label="time" aria-describedby="basic-addon1"  onChange={this.timeChangeHandler} pattern=".*\S.*" required />
</div>
<div className="input-group mb-2">
    <div className="input-group-prepend">
        <span className="input-group-text" id="basic-addon1"><b>City</b></span>
    </div>
    <input type="text" size="50" name="city" className="form-control" aria-label="city" aria-describedby="basic-addon1" onChange={this.cityChangeHandler}   pattern=".*\S.*" required />
</div>
<div className="input-group mb-2">
    <div className="input-group-prepend">
        <span className="input-group-text" id="basic-addon1"><b>Location</b></span>
    </div>
    <input type="text" size="50" name="location" className="form-control" aria-label="location" aria-describedby="basic-addon1" onChange={this.locationChangeHandler}   pattern=".*\S.*" required />
</div>
<div className="input-group mb-2">
    <div className="input-group-prepend">
        <span className="input-group-text" id="basic-addon1"><b>Date</b></span>
    </div>
    <input type="date" size="50" name="Date" className="form-control" aria-label="Date" aria-describedby="basic-addon1" onChange={this.dateChangeHandler}   pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" placeholder="YYYY-MM-DD" title="Enter a date in this formart YYYY-MM-DD" required />
</div>

<center>
    <Button variant="primary" type="submit">
        <b>Add</b>
    </Button>&nbsp;&nbsp;
    <Button variant="secondary" onClick={this.closeAddModal}>
        <b>Close</b>
    </Button>
</center>
</div>
</div>
</form>
</div>

</Modal>
<Modal
                            isOpen={this.state.openStudent}
                            onRequestClose={this.closeStudentModal}
                             contentLabel="Example Modal" >
                                 
                                 <div className="row mt-3">
                  <div className="col-sm-4">
                      <div className="card" style={{width: 15 +"rem"}}>
                          <img className="card-img-top" src={userImage} alt="" />
                          <div className="text-center">
                          <div className="card-body">
                          <div class="panel panel-default">
                    <div class="panel-heading">Contact</div>
                    <div class="panel-body">{this.state.student_profile.emailId}</div>
                    <div class="panel-body">{this.state.student_profile.contactNumber}</div>
                    <div class="panel-body">{this.state.student_profile.city}</div>
                    <div class="panel-body">{this.state.student_profile.state}</div>
                    <div class="panel-body">{this.state.student_profile.country}</div>
                    </div>
                       </div>
                      </div>
                      </div>
                  </div>
                  
                  <div className="col-sm-7">
                  <div class="panel panel-default">
                    <div class="panel-heading">About</div>
                    <div class="panel-body">{this.state.student_profile.name}</div>
                    <div class="panel-body">{this.state.student_profile.collegeName}</div>
                    <div class="panel-body">{this.state.student_profile.major}</div>

                </div>
                    
                    <div class="panel panel-default">
                    <div class="panel-heading">My Journey</div>
                 <div class="panel-body">{this.state.student_profile.careerObjective}</div>
                </div>
                {this.state.educationalDetails.map(education =>
                    <div class="panel panel-default">
                    <div class="panel-heading">Educational Details</div>
                    <div key={education._id}>
                    <div class="panel-body">College Name: {education.collegeName}</div>
                    <div class="panel-body">College Location: {education.location}</div>
                    <div class="panel-body">Major: {education.major}</div>
                    <div class="panel-body">Degree: {education.degree}</div>
                    <div class="panel-body">CGPA: {education.cgpa}</div>
                    <div class="panel-body">Year of passing:{education.yop}</div>
                    </div>
                </div>
                    )
                    }
                    {this.state.experienceDetails.map(experience =>
                <div class="panel panel-default">
                    <div class="panel-heading">Experience Details</div>
                    <div key={experience._id}>
                    <div class="panel-body">Company Name: {experience.companyName}</div>
                    <div class="panel-body">Title: {experience.title}</div>
                    <div class="panel-body">Location: {experience.location}</div>
                    <div class="panel-body">Start Date: {experience.startDate}</div>
                    <div class="panel-body">End Date: {experience.endDate}</div>
                    <div class="panel-body">Work Description:{experience.workDescription}</div>
                    </div>
                    </div> 
                    )}
                 
                <div class="panel panel-default">
                    <div class="panel-heading">Skills</div>
                     <div class="panel-body">{this.state.student_profile.skills}</div>
                    
                </div>
                      
                  </div>
              </div>         
                         <center>
                                <Button variant="primary" onClick={this.closeStudentModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                        </Modal>
</div>
</div>
           
            
    )
  }
}
const mapStateToProps=state=>{
    return{
        events:state.events
    };
}    
const mapDispatchToProps = dispatch=> {
    return{
        getEvents: () =>dispatch ({type:'EVENTDETAILS'})
    };
    }
export default connect(mapStateToProps,mapDispatchToProps)(EventsCompany);