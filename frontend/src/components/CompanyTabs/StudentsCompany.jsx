import React, {Component} from 'react';
import '../../App.css';
import axios from 'axios';
import {Redirect} from 'react-router';
import {connect} from 'react-redux';
import {Button} from 'react-bootstrap';
import Modal from 'react-modal';
import dummy from '../../common/dummy.png';
import {backendURI} from '../../common/config';
import { graphql} from 'react-apollo';
import {getStudentsQuery } from '../../queries/queries';
import * as compose from 'lodash.flowright';
import {getStudentDetailsMutation } from '../../mutation/mutation';
class StudentsCompany extends Component {
    constructor(props) {
        super(props);
        this.state = {
            successUpdate: false,
            searchstring:'',
            students:[] ,
            openStudent:false ,
            student_profile:[],
            educationalDetails:[],
            experienceDetails:[] ,
            pageIndex:1,
            openMessage:false
        };
        this.openStudent = this.openStudent.bind(this);
        this.closeModal = this.closeModal.bind(this);
        // this.sendMessage = this.sendMessage.bind(this);
        this.cancelModal = this.cancelModal.bind(this);
        //this.submitMessage = this.submitMessage.bind(this);
    }
    componentDidMount(){
        let students=[];
      students=this.props.getStudentsQuery;
    }
 async openStudent(student){
        this.setState({
            openStudent: true ,
            studentid:student._id   
        });
        let mutationResponse =  await this.props.getStudentDetailsMutation({
            variables: {
                studentid:student.id
                }, 
    });
    let student_profile = mutationResponse.data.student;
    console.log(student_profile)
    let educationalDetails=mutationResponse.data.student.educationalDetails;
    let experienceDetails=mutationResponse.data.student.experienceDetails;
    this.setState({student_profile,educationalDetails,experienceDetails});
    

    }
    closeModal() {
        this.setState({
            openStudent:false
        });
    }
    cancelModal() {
        this.setState({
            openMessage:false
        });
    }
  
  
    

    studentCriteria=(e)=>
    {
        this.setState({
            searchstring : e.target.value
        })
    }
  
    searchStudent=()=>
    {
        let data = {
            searchString:this.state.searchstring ,
            pageIndex:this.state.pageIndex  
        }
        axios.post(backendURI +'/profile/searchStudentsCompany',data)
        .then(response => {
            console.log("Status Code : ",response.status);
            if(response.status === 200){
                let students=response.data;
                
                console.log(JSON.stringify(students))
                this.setState({
                    students   
                });
                localStorage.setItem("students",JSON.stringify(students));
                this.props.getStudentsForCompany();
            }
        })
        .catch(err => { 
            this.setState({errorMessage:"Students cannot be viewed"});
        });
    }
    
   
    render() {
        let redirectVar;let studentlist;let students=[];
        if (!localStorage.getItem("token")) {
            redirectVar = <Redirect to="/login" />;
        }
        if(this.props.data.students)
        {
            students = this.props.data.students;    
        }   
        console.log("line171",this.props.data.students)
        let userImage=this.state.student_profile.image||dummy
        studentlist =( <div className="panel panel-default p50 uth-panel">
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Student Name</th>
                            <th>Student Email Id</th>
                            <th>College Name</th>
                            <th>Skills</th>
                        </tr>
                    </thead>
                    <tbody>
                    {students.map(student =>
                        <tr key={student._id}>
                        <td><a onClick={() => this.openStudent(student)}>{student.name}</a></td>
                        <td>{student.emailId}</td>
                        <td>{student.collegeName}</td>
                        <td>{student.skills}</td>
                        </tr>
                    )}
                     </tbody>
                </table>
               
                </div>)
       
        return (
            <div>
            {redirectVar}
            
            <div className="container">
                    
                        <div className="main-div">
                            <div className="panel">
                                <h2>Student Search</h2>
                                  
    <div class="row">    
        <div class="col-xs-8 col-xs-offset-2">
		    <div>
                <div class="input-group-btn search-panel">
                   
                  
                </div>
             
                <input type="text" class="form-control" name="search" placeholder="Student Name or College Name or Skill" onChange={this.studentCriteria}/>
                <div style={{display: "flex",justifyContent: "center",alignItems: "center"}}>
                    <button class="btn btn-primary" type="button" onClick={this.searchStudent}><span class="glyphicon glyphicon-search"></span>Search</button>
                </div>
            </div>
        </div>
	</div>
    
</div>
</div>
{studentlist}

<Modal
                            isOpen={this.state.openStudent}
                            onRequestClose={this.closeModal}
                             contentLabel="Example Modal" >
                                 
                                 <div className="row mt-3">
                  <div className="col-sm-4">
                      <div className="card" style={{width: 15 +"rem"}}>
                          <img className="card-img-top" src={userImage} alt="" />
                          <div className="text-center">
                          <div className="card-body">
                          <div class="panel panel-default">
                    <div class="panel-heading">Contact</div>
                    <div class="panel-body">{this.state.student_profile.emailId}</div>
                    <div class="panel-body">{this.state.student_profile.contactNumber}</div>
                    <div class="panel-body">{this.state.student_profile.city}</div>
                    <div class="panel-body">{this.state.student_profile.state}</div>
                    <div class="panel-body">{this.state.student_profile.country}</div>
                    </div>
                       </div>
                      </div>
                      </div>
                      
                  </div>
                  
                  <div className="col-sm-7">
                  <div class="panel panel-default">
                    <div class="panel-heading">About</div>
                    <div class="panel-body">{this.state.student_profile.name}</div>
                    <div class="panel-body">{this.state.student_profile.collegeName}</div>
                    <div class="panel-body">{this.state.student_profile.major}</div>

                </div>
                    
                    <div class="panel panel-default">
                    <div class="panel-heading">My Journey</div>
                 <div class="panel-body">{this.state.student_profile.careerObjective}</div>
                </div>
                {this.state.educationalDetails.map(education =>
                    <div class="panel panel-default">
                    <div class="panel-heading">Educational Details</div>
                    <div key={education._id}>
                    <div class="panel-body">College Name: {education.collegeName}</div>
                    <div class="panel-body">College Location: {education.location}</div>
                    <div class="panel-body">Major: {education.major}</div>
                    <div class="panel-body">Degree: {education.degree}</div>
                    <div class="panel-body">CGPA: {education.cgpa}</div>
                    <div class="panel-body">Year of passing:{education.yop}</div>
                    </div>
                </div>
                    )
                    }
                    {this.state.experienceDetails.map(experience =>
                <div class="panel panel-default">
                    <div class="panel-heading">Experience Details</div>
                    <div key={experience._id}>
                    <div class="panel-body">Company Name: {experience.companyName}</div>
                    <div class="panel-body">Title: {experience.title}</div>
                    <div class="panel-body">Location: {experience.location}</div>
                    <div class="panel-body">Start Date: {experience.startDate}</div>
                    <div class="panel-body">End Date: {experience.endDate}</div>
                    <div class="panel-body">Work Description:{experience.workDescription}</div>
                    </div>
                    </div> 
                    )}
                 
                <div class="panel panel-default">
                    <div class="panel-heading">Skills</div>
                     <div class="panel-body">{this.state.student_profile.skills}</div> 
                </div>    
                  </div>
              </div>     
                         <center>
                            
                         {/* <Button variant="primary" onClick={() => this.sendMessage(this.state.student_profile)}>
                                    <b>Send Message</b>
                                </Button>{" "} */}
                                <Button variant="primary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                               
                            </center>
                        </Modal>

                        <Modal
                            isOpen={this.state.openMessage}
                            onRequestClose={this.cancelModal}
                             contentLabel="Example Modal" >
                                 
                 <form onSubmit={()=>this.submitMessage()}>              
                <div class="panel panel-default">
                    <div class="panel-heading">Message: </div>
                     <div class="panel-body">
                     <div className="input-group mb-2">
    <div className="input-group-prepend">
    </div>
    <input type="text" size="50" name="message" className="form-control" aria-label="description" aria-describedby="basic-addon1" onChange={this.messageContentHandler} 
     pattern=".*\S.*" required />
</div>
                     </div>
                </div>
                         <center>
                         <Button variant="primary" type="submit">
                                    <b>Send</b>
                                </Button>
                                <Button variant="primary" onClick={this.cancelModal}>
                                    <b>Cancel</b>
                                </Button>
                               
                            </center>
                            </form>
                        </Modal>
</div>
 </div>
           
    )
  }
}

export default compose(graphql(getStudentsQuery),

graphql(getStudentDetailsMutation, { name: "getStudentDetailsMutation" }))
    
(StudentsCompany);