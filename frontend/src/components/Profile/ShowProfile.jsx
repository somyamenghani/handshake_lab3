import React, {Component} from 'react';
import '../../App.css';
import axios from 'axios';
import {Redirect} from 'react-router';
import dummy from '../../common/dummy.png';
import {connect} from 'react-redux';
import Modal from 'react-modal';
import { Button } from 'react-bootstrap';
import {backendURI} from '../../common/config';
import { graphql} from 'react-apollo';
import {getStudentDetailsQuery } from '../../queries/queries';
import {updateStudentMutation } from '../../mutation/mutation';
import {addEducationMutation } from '../../mutation/mutation';
import {addExperienceMutation } from '../../mutation/mutation';
import * as compose from 'lodash.flowright';
class ShowProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showEditModal: false,
            user_profile:[],
            educationalDetails:[],
            experienceDetails:[],
            imageModal:false,
            userImage:null,
            imageChange:false,
            showEducationModal:false,
            showEducationEdit:false,
            showExperienceModal:false,
            showExperienceEdit:false,
            educationId:'',
            successFlag:false,
            successFlagDeletion:false
        };
        this.handleEdit = this.handleEdit.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.handleImageEdit = this.handleImageEdit.bind(this);
        this.onImageSubmit=this.onImageSubmit.bind(this);
        this.handleEducationEdit = this.handleEducationEdit.bind(this);
        this.handleExperienceEdit = this.handleExperienceEdit.bind(this);
        this.handleExperienceAdd = this.handleExperienceAdd.bind(this);
    }
    
   
    componentWillMount() {
      
       let user_profile;
       user_profile=this.props.getStudentDetailsQuery;
        
    }
    

    handleEdit = ()=>{

this.setState({ showEditModal:true});
        
    }
    handleImageEdit=(e)=>{
        this.setState({imageModal:true
            
        });
        
    }
    handleEducationAdd=(e)=>{
        this.setState({ showEducationModal:true
            
        });
    }
    handleEducationEdit=(e)=>{
        this.setState({ showEducationEdit:true,
            educationId:e._id

     });
    }
    handleEducationDeletion=async(e)=>{
        await this.setState({
            educationId:e._id
     });
     
        let userId=localStorage.getItem("user_id");
        const data={
            userId:userId,
            educationId:this.state.educationId
        }
        console.log("data going to education deletion"+JSON.stringify(data));
        //set the with credentials to true
        axios.defaults.withCredentials = true;
        axios.post(backendURI +'/update/deleteEducationDetails',data)
        .then(response => {
        if (response.status === 200) {
            console.log(response.data);
            this.setState({
                successFlagDeletion:true
            })
            this.getProfile();
        }
        
        
    })
    .catch(err => { 
        this.setState({errorMessage: "error"});
    })
}
    onEducationSubmit= async(e)=>{
        e.preventDefault();
        let userId=localStorage.getItem("user_id");
        let mutationResponse = await this.props.addEducationMutation({
            variables: {
                userId:userId,
            userCollege:this.state.userCollege,
            userMajor:this.state.userMajor,
            userDegree:this.state.userDegree,
            userCgpa:this.state.userCgpa,
            userCollegeLocation:this.state.userCollegeLocation,
            userYOP:this.state.userYOP
                
            },
            refetchQueries: [{ query: getStudentDetailsQuery ,
                variables: { studentId: localStorage.getItem("user_id") }}]
        
        });
        let response = mutationResponse.data.addEducation;
        console.log("line121",response)
        this.setState({
                        showEducationModal: false
                    });    
    }
    onEducationEdit= async(e)=>{
        e.preventDefault();
        let userId=localStorage.getItem("user_id");
        const data={
            userId:userId,
            educationId:this.state.educationId,
            userCollege:this.state.userCollege,
            userMajor:this.state.userMajor,
            userDegree:this.state.userDegree,
            userCgpa:this.state.userCgpa,
            userCollegeLocation:this.state.userCollegeLocation,
            userYOP:this.state.userYOP
        }
        console.log("data going to edit education details"+JSON.stringify(data));
        //set the with credentials to true
        axios.defaults.withCredentials = true;
        axios.post(backendURI +'/update/editEducationDetails',data)
        .then(response => {
        if (response.status === 200) {
            console.log(response.data);
            this.setState({
                showEducationEdit: false
            });
            this.getProfile();
        }
    })
    .catch(err => { 
        this.setState({errorMessage: "error"});
    })   
    }
    handleExperienceAdd=(e)=>{
        this.setState({ showExperienceModal:true
            
        });
    }
    handleExperienceEdit=(e)=>{
        this.setState({ showExperienceEdit:true,
            experienceId:e._id

     });
    }
    handleExperienceDeletion=async(e)=>{
        await this.setState({
            experienceId:e._id
     });
     
        let userId=localStorage.getItem("user_id");
        const data={
            userId:userId,
            experienceId:this.state.experienceId
        }
        console.log("data going to experience deletion"+JSON.stringify(data));
        //set the with credentials to true
        axios.defaults.withCredentials = true;
        axios.post(backendURI +'/update/deleteExperienceDetails',data)
        .then(response => {
        if (response.status === 200) {
            console.log(response.data);
            this.setState({
                successFlagDeletion:true
            })
            this.getProfile();
        }
        
        
    })
    .catch(err => { 
        this.setState({errorMessage: "error"});
    })
}
    onExperienceSubmit= async(e)=>{
        e.preventDefault();
        let userId=localStorage.getItem("user_id");

        let mutationResponse = await this.props.addExperienceMutation({
            variables: {
                userId:userId,
            companyName:this.state.companyName,
            title:this.state.title,
            location:this.state.location,
            startDate:this.state.startDate,
            endDate:this.state.endDate,
            workDescription:this.state.workDescription
                
            },
            refetchQueries: [{ query: getStudentDetailsQuery ,
                variables: { studentId: localStorage.getItem("user_id") }}]
        
        });
        let response = mutationResponse.data.addExperience;
        console.log("line121",response)
        this.setState({
                        showExperienceModal: false
                    }); 
   
        
    }
    onExperienceEdit= async(e)=>{
        e.preventDefault();
        let userId=localStorage.getItem("user_id");
        const data={
            userId:userId,
            experienceId:this.state.experienceId,
            companyName:this.state.companyName,
            title:this.state.title,
            location:this.state.location,
            startDate:this.state.startDate,
            endDate:this.state.endDate,
            workDescription:this.state.workDescription
        }
        console.log("data going to edit experience details"+JSON.stringify(data));
        //set the with credentials to true
        axios.defaults.withCredentials = true;
        axios.post(backendURI +'/update/editExperienceDetails',data)
        .then(response => {
        if (response.status === 200) {
            console.log(response.data);
            this.setState({
                showExperienceEdit: false
            });
            this.getProfile();
        }
    })
    .catch(err => { 
        this.setState({errorMessage: "error"});
    })   
    }
    closeModal() {
        this.setState({
            showEditModal:false,
            imageModal:false,
            showEducationModal:false,
            showEducationEdit:false,
            showExperienceModal:false,
            showExperienceEdit:false
        });
    }
    userNameChangeHandler = (e) => {
        this.setState({
            userName: e.target.value
        });
    };
    careerObjChange = (e) => {
        this.setState({
            userCareerObj: e.target.value
        });
    };
    contactChange = (e) => {
        this.setState({
            userContactNumber: e.target.value
        });
    };
    skillChange = (e) => {
        this.setState({
            userSkills: e.target.value
        });
    };
    companyNameChange = (e) => {
        this.setState({
            companyName: e.target.value
        });
    };
    titleChange = (e) => {
        this.setState({
            title: e.target.value
        });
    };
    locationChange = (e) => {
        this.setState({
            location: e.target.value
        });
    };
    startDateChange = (e) => {
        this.setState({
            startDate: e.target.value
        });
    };
    endDateChange = (e) => {
        this.setState({
            endDate: e.target.value
        });
    };
    workDescriptionChange = (e) => {
        this.setState({
            workDescription: e.target.value
        });
    };
    collegeChange = (e) => {
        this.setState({
            userCollege: e.target.value
        });
    };
    majorChange = (e) => {
        this.setState({
            userMajor: e.target.value
        });
    };
    degreeChange = (e) => {
        this.setState({
            userDegree: e.target.value
        });
    };
    cgpaChange = (e) => {
        this.setState({
            userCgpa: e.target.value
        });
    };
    collegeLocationChange = (e) => {
        this.setState({
            userCollegeLocation: e.target.value
        });
    };
    yopChange = (e) => {
        this.setState({
            userYOP: e.target.value
        });
    };
    cityChange = (e) => {
        this.setState({
            userCity: e.target.value
        });
    };
    stateChange = (e) => {
        this.setState({
            userState: e.target.value
        });
    };
    countryChange = (e) => {
        this.setState({
            userCountry: e.target.value
        });
    };
    handleImageChange = async(e) => {
        console.log(e.target.files[0])
       await this.setState({
            userImage: e.target.files[0]
        });
    };
    onImageSubmit= async (e)=>{
        
        const data = new FormData()
        data.append('file', this.state.userImage);
        data.append('userId',localStorage.getItem("user_id"));
       await axios.post(backendURI +'/profile/upload',data)
        .then(response => {
        if (response.status === 200) {
            console.log("Image uploaded")
            this.setState({
                imageModal: false,
                imageChange:true
            });
            this.getProfile();
        }
       
       
    }
    )
    .catch(err => { 
        this.setState({errorMessage: "error"});
    })
    await this.getProfile();
    }
    onSubmit = async (e) => {
        e.preventDefault();
        let mutationResponse = await this.props.updateStudentMutation({
            variables: {
            studentId:localStorage.getItem("user_id"),
            name:this.state.userName||this.props.data.student.name,
   careerObjective:this.state.userCareerObj||this.props.data.student.careerObjective,
   collegeName:this.state.userCollege,
   skills:this.state.userSkills||this.props.data.student.skills,
    
   
        },
        refetchQueries: [{ query: getStudentDetailsQuery ,
            variables: { studentId: localStorage.getItem("user_id") }}]
    });
    let response = mutationResponse.data.updateStudent;
    this.setState({showEditModal: false})
    console.log("line146",response)

   
}

    render() {
        let redirectVar;let student;let name,emailId,collegeName,major,careerObjective,skills;let educationalDetails=[];let experienceDetails=[];
        let userImage =this.state.user_profile.image||dummy;
        if (!localStorage.getItem("token")) {
            redirectVar = <Redirect to="/login" />;
        }
        else
        {
            redirectVar = <Redirect to="/showProfile" />
        }

        if(this.props.data.student)
        {
            student=this.props.data.student;
           console.log("line465",this.props.data.student)  
            name=this.props.data.student.name;
            emailId=this.props.data.student.emailId;
            collegeName=this.props.data.student.collegeName;
            major=this.props.data.student.major;
            careerObjective=this.props.data.student.careerObjective;
            skills=this.props.data.student.skills;
            educationalDetails=this.props.data.student.educationalDetails;
            experienceDetails=this.props.data.student.experienceDetails;
        }    
        return (
            <div>
            {redirectVar}
            <div class="container">
            <div className="row mt-3">
                  <div className="col-sm-4">
                      <div className="card" style={{width: 15 +"rem"}}>
                          <img className="card-img-top" src={userImage} alt="" />
                         
                          <div className="text-center">
                          <div className="card-body">
                          <div class="panel panel-default">
                    <div class="panel-heading">Contact</div>
                    <div class="panel-body">{emailId}</div>
                    <div class="panel-body">{this.state.user_profile.contactNumber}</div>
                    <div class="panel-body">{this.state.user_profile.city}</div>
                    <div class="panel-body">{this.state.user_profile.state}</div>
                    <div class="panel-body">{this.state.user_profile.country}</div>
                    </div>
                       </div>
                      </div>
                      </div>
                  </div>
                  
                  <div className="col-sm-7">
                  <div class="panel panel-default">
                    <div class="panel-heading">About</div>
                    <div class="panel-body">{name}</div>
                    <div class="panel-body">{collegeName}</div>
                    <div class="panel-body">{major}</div>

                </div>
                    
                    <div class="panel panel-default">
                    <div class="panel-heading">My Journey</div>
                 <div class="panel-body">{careerObjective}</div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">Skills</div>
                     <div class="panel-body">{skills}</div> 
                     <div class="panel-footer text-right">
                 <button type="button" id="profileEdit" class="btn btn-primary btn-block pull-right" onClick={this.handleEdit}>Edit Basic Details</button>
                 <button type="button" id="educationAdd" class="btn btn-primary btn-block pull-right" onClick={this.handleEducationAdd}>Add Education</button>
                 
                    </div>
                </div>
                    {educationalDetails.map(education =>
                    <div class="panel panel-default">
                    <div class="panel-heading">Educational Details</div>
                    <div key={education._id}>
                    <div class="panel-body">College Name: {education.collegeName}</div>
                    <div class="panel-body">College Location: {education.location}</div>
                    <div class="panel-body">Major: {education.major}</div>
                    <div class="panel-body">Degree: {education.degree}</div>
                    <div class="panel-body">CGPA: {education.cgpa}</div>
                    <div class="panel-body">Year of passing:{education.yop}</div>
                    </div>
                    <div class="panel-footer">
                     {/* <button type="button" id="educationDelete" class="btn btn-danger pull-right" onClick={() => this.handleEducationDeletion(education)}>Delete Education</button>
                     <button type="button" id="educationEdit" class="btn btn-success pull-right" onClick={() => this.handleEducationEdit(education)}>Edit Education</button> */}
                     </div>
                </div>
                    )
                    }
                    
                    
                    <button type="button" id="experienceAdd" class="btn btn-primary btn-block pull-right" onClick={this.handleExperienceAdd}>Add Experience</button>
                    {experienceDetails.map(experience =>
                <div class="panel panel-default">
                    <div class="panel-heading">Experience Details</div>
                    <div key={experience._id}>
                    <div class="panel-body">Company Name: {experience.companyName}</div>
                    <div class="panel-body">Title: {experience.title}</div>
                    <div class="panel-body">Location: {experience.location}</div>
                    <div class="panel-body">Start Date: {experience.startDate}</div>
                    <div class="panel-body">End Date: {experience.endDate}</div>
                    <div class="panel-body">Work Description:{experience.workDescription}</div>
                    </div>
        <div class="panel-footer">
                     {/* <button type="button" id="experienceDelete" class="btn btn-danger pull-right" onClick={() =>this.handleExperienceDeletion(experience)}>Delete Experience</button>
                     <button type="button" id="experienceEdit" class="btn btn-success pull-right" onClick={() => this.handleExperienceEdit(experience)}>Edit Experience</button> */}
                     </div>
                    </div> 
                    )}
                  </div> 
              </div> 
              
  
</div> 
{/* Basic details edit modal */}
<Modal
       isOpen={this.state.showEditModal}
       onRequestClose={this.closeModal}
        contentLabel="Example Modal" >
      <div>             
      <form onSubmit={this.onSubmit}>
            <div class="container">
            <div class="panel panel-default">
                    <div class="panel-heading">Let us know more about you...</div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Username</b></span>
                                </div>
                                <input type="text" size="50" name="user_name" className="form-control" aria-label="Username" aria-describedby="basic-addon1" onChange={this.userNameChangeHandler} defaultValue={name}  pattern=".*\S.*" title="Please enter a unique user name.Username cannot be spaces" required />
                            </div>

                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Career Objective</b></span>
                                </div>
                                <input type="text" size="50" name="careerObjective" className="form-control" aria-label="careerObjective" aria-describedby="basic-addon1" onChange={this.careerObjChange} defaultValue=
                                {careerObjective}  pattern=".*\S.*"/>
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Email</b></span>
                                </div>
                                <input type="email" size="50" name="email_id" className="form-control" aria-label="Email" aria-describedby="basic-addon1" defaultValue=
                                {emailId} readOnly />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Contact Number</b></span>
                                </div>
                                <input type="number" size="50" name="contactNumber" className="form-control" aria-label="ContactNumber" aria-describedby="basic-addon1" onChange={this.contactChange} defaultValue= {this.state.user_profile.contactNumber} pattern=".*\S.*" />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>City</b></span>
                                </div>
                                <input type="text"size="50" name="city" className="form-control" aria-label="City" aria-describedby="basic-addon1" onChange={this.cityChange} defaultValue= {this.state.user_profile.city}  pattern=".*\S.*"  />
                            </div>

                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>State</b></span>
                                </div>
                                <input type="text"size="50" name="state" className="form-control" aria-label="State" aria-describedby="basic-addon1" onChange={this.stateChange} defaultValue={this.state.user_profile.state}  pattern=".*\S.*" />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Country</b></span>
                                </div>
                                <input type="text"size="50" name="country" className="form-control" aria-label="country" aria-describedby="basic-addon1" onChange={this.countryChange} defaultValue= {this.state.user_profile.country} pattern=".*\S.*" />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Skills</b></span>
                                </div>
                                <input type="text"size="50" name="skills" className="form-control" aria-label="skills" aria-describedby="basic-addon1" onChange={this.skillChange} defaultValue= {skills} pattern=".*\S.*"  />
                            </div>
                            <center>
                                <Button variant="primary" type="submit">
                                    <b>Update</b>
                                </Button>&nbsp;&nbsp;
                                <Button variant="secondary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                            </div>
                            </div>
                        </form>
    </div>
   </Modal>
   {/* Education details add modal */}
   <Modal
       isOpen={this.state.showEducationModal}
       onRequestClose={this.closeModal}
        contentLabel="Example Modal" >
      <div>             
      <form onSubmit={this.onEducationSubmit}>
            <div class="container">
            <div class="panel panel-default">
            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>College Name</b></span>
                                </div>
                                <input type="text" size="50" name="collegeName" className="form-control" aria-label="collegeName" aria-describedby="basic-addon1" onChange={this.collegeChange}  pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text"  id="basic-addon1"><b>Major</b></span>
                                </div>
                                <input type="text" name="major" size="50"className="form-control" aria-label="major" aria-describedby="basic-addon1" onChange={this.majorChange} pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Degree</b></span>
                                </div>
                                <input type="text" size="50" name="degree" className="form-control" aria-label="degree" aria-describedby="basic-addon1" onChange={this.degreeChange} pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>CGPA</b></span>
                                </div>
                        
                                <input type="number" size="50" name="cgpa" step="0.01" className="form-control" aria-label="cgpa" aria-describedby="basic-addon1" onChange={this.cgpaChange} required  />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>College Location</b></span>
                                </div>
                                <input type="text" size="50" name="collegeLocation" className="form-control" aria-label="collegeLocation" aria-describedby="basic-addon1" onChange={this.collegeLocationChange} pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Year of passing</b></span>
                                </div>
                                <input type="number"size="50" name="yop" className="form-control" aria-label="yop" aria-describedby="basic-addon1" onChange={this.yopChange}  required />
                            </div>
                            <center>
                                <Button variant="primary" type="submit">
                                    <b>Add</b>
                                </Button>&nbsp;&nbsp;
                                <Button variant="secondary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                            </div>
                            </div>
                        </form>
    </div>
   </Modal>
   {/* Education details edit modal */}
   <Modal
       isOpen={this.state.showEducationEdit}
       onRequestClose={this.closeModal}
        contentLabel="Example Modal" >
      <div>             
      <form onSubmit={this.onEducationEdit}>
            <div class="container">
            <div class="panel panel-default">
            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>College Name</b></span>
                                </div>
                                <input type="text" size="50" name="collegeName" className="form-control" aria-label="collegeName" aria-describedby="basic-addon1" onChange={this.collegeChange}  pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text"  id="basic-addon1"><b>Major</b></span>
                                </div>
                                <input type="text" name="major" size="50"className="form-control" aria-label="major" aria-describedby="basic-addon1" onChange={this.majorChange} pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Degree</b></span>
                                </div>
                                <input type="text" size="50" name="degree" className="form-control" aria-label="degree" aria-describedby="basic-addon1" onChange={this.degreeChange} pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>CGPA</b></span>
                                </div>
                        
                                <input type="number" size="50" name="cgpa" step="0.01" className="form-control" aria-label="cgpa" aria-describedby="basic-addon1" onChange={this.cgpaChange} required  />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>College Location</b></span>
                                </div>
                                <input type="text" size="50" name="collegeLocation" className="form-control" aria-label="collegeLocation" aria-describedby="basic-addon1" onChange={this.collegeLocationChange} pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Year of passing</b></span>
                                </div>
                                <input type="number"size="50" name="yop" className="form-control" aria-label="yop" aria-describedby="basic-addon1" onChange={this.yopChange}  required />
                            </div>
                            <center>
                                <Button variant="primary" type="submit">
                                    <b>Update</b>
                                </Button>&nbsp;&nbsp;
                                <Button variant="secondary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                            </div>
                            </div>
                        </form>
    </div>
   </Modal>
   {/* Experience details add modal */}
   <Modal
       isOpen={this.state.showExperienceModal}
       onRequestClose={this.closeModal}
        contentLabel="Example Modal" >
      <div>             
      <form onSubmit={this.onExperienceSubmit}>
            <div class="container">
            <div class="panel panel-default">
            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Company Name</b></span>
                                </div>
                                <input type="text" size="50" name="companyName" className="form-control" aria-label="companyName" aria-describedby="basic-addon1" onChange={this.companyNameChange}  pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text"  id="basic-addon1"><b>Title</b></span>
                                </div>
                                <input type="text" name="title" size="50"className="form-control" aria-label="title" aria-describedby="basic-addon1" onChange={this.titleChange} pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Location</b></span>
                                </div>
                                <input type="text" size="50" name="location" className="form-control" aria-label="location" aria-describedby="basic-addon1" onChange={this.locationChange} pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Start Date</b></span>
                                </div>
                        
                                <input type="date" size="50" name="startDate" className="form-control" aria-label="startDate" aria-describedby="basic-addon1" onChange={this.startDateChange} required  />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>End Date</b></span>
                                </div>
                                <input type="date" size="50" name="endDate" className="form-control" aria-label="endDate" aria-describedby="basic-addon1" onChange={this.endDateChange} pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Work Description</b></span>
                                </div>
                                <input type="text"size="50" name="workDescription" className="form-control" aria-label="workDescription" aria-describedby="basic-addon1" onChange={this.workDescriptionChange}  required />
                            </div>
                
                            <center>
                                <Button variant="primary" type="submit">
                                    <b>Add</b>
                                </Button>&nbsp;&nbsp;
                                <Button variant="secondary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                            </div>
                            </div>
                        </form>
    </div>
   </Modal>
   {/* Experience details edit modal */}
   <Modal
       isOpen={this.state.showExperienceEdit}
       onRequestClose={this.closeModal}
        contentLabel="Example Modal" >
      <div>             
      <form onSubmit={this.onExperienceEdit}>
            <div class="container">
            <div class="panel panel-default">
            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Company Name</b></span>
                                </div>
                                <input type="text" size="50" name="companyName" className="form-control" aria-label="companyName" aria-describedby="basic-addon1" onChange={this.companyNameChange}  pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text"  id="basic-addon1"><b>Title</b></span>
                                </div>
                                <input type="text" name="title" size="50"className="form-control" aria-label="title" aria-describedby="basic-addon1" onChange={this.titleChange} pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Location</b></span>
                                </div>
                                <input type="text" size="50" name="location" className="form-control" aria-label="location" aria-describedby="basic-addon1" onChange={this.locationChange} pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Start Date</b></span>
                                </div>
                        
                                <input type="date" size="50" name="startDate" className="form-control" aria-label="startDate" aria-describedby="basic-addon1" onChange={this.startDateChange} required  />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>End Date</b></span>
                                </div>
                                <input type="date" size="50" name="endDate" className="form-control" aria-label="endDate" aria-describedby="basic-addon1" onChange={this.endDateChange} pattern=".*\S.*" required />
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1"><b>Work Description</b></span>
                                </div>
                                <input type="text"size="50" name="workDescription" className="form-control" aria-label="workDescription" aria-describedby="basic-addon1" onChange={this.workDescriptionChange}  required />
                            </div>
                            <center>
                                <Button variant="primary" type="submit">
                                    <b>Update</b>
                                </Button>&nbsp;&nbsp;
                                <Button variant="secondary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                            </div>
                            </div>
                        </form>
    </div>
   </Modal>

   <Modal
                            isOpen={this.state.imageModal}
                            onRequestClose={this.closeModal}
                             contentLabel="Example Modal" >
                           
                           <div>
                         <form onSubmit={this.onImageSubmit} enctype="multipart/form-data">
                             
            <div class="container">
            <div class="panel panel-default">
    <div class="panel-heading">Choose profile picture: </div>
                    <div className="input-group mb-2">
                                <input type="file" name="user_image" accept="image/*" className="form-control" aria-label="Image" aria-describedby="basic-addon1" onChange={this.handleImageChange} />
                            </div>
                            <center>
                                <Button variant="primary" type="submit">
                                    <b>Change</b>
                                </Button>&nbsp;&nbsp;
                                <Button variant="secondary" onClick={this.closeModal}>
                                    <b>Close</b>
                                </Button>
                            </center>
                            </div>
                            </div>
                        </form>
                        </div>
                        </Modal>
      </div>
       
    )
  }
}

//export Show Profile Component
export default compose(
    graphql(updateStudentMutation, { name: "updateStudentMutation" }),
    graphql(addEducationMutation, { name: "addEducationMutation" }),
    graphql(addExperienceMutation, { name: "addExperienceMutation" }),
    graphql(getStudentDetailsQuery, {
        options: {
            variables: { studentId: localStorage.getItem("user_id") }
        }
    }
   
))(ShowProfile);

