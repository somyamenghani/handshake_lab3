Handshake Prototype Lab 3 - Using Node js, React js, Mongo DB, Graphql

How to run application:

for server:
cd backend
npm install
node index.js

for client side:
cd frontend
npm install
npm start

to access graphql console:
http://localhost:3001/graphql
